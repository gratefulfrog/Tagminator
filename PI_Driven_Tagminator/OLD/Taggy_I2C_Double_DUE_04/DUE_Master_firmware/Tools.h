#ifndef TOOLS_H
#define TOOLS_H

#include <Arduino.h>

extern void delayMS(unsigned long del);
extern char getChar(int i);

#endif
