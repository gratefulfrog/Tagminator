#include "SlaveApp.h"

SlaveApp* SlaveApp::thisPtr;

void SlaveApp::receiveEvent(int howMany){
  thisPtr->incoming  = Wire.read();    // receive byte as an signed char
}

SlaveApp::SlaveApp(){
  SlaveApp::thisPtr = this;
  Wire.begin(SLAVE_ADDRESS);                // join i2c bus with address #8
  Wire.onReceive(SlaveApp::receiveEvent);             // register event
  _aStepper = new AStepper();

#ifdef DEBUG_DUE_SLAVE
  Serial.begin(115200);
  //while(!Serial);
#endif
}

void SlaveApp::mainLoop(){
  static signed char localPCT = 0;
  
  if(_aStepper->getSpeedAsPct()){
    _aStepper->step();
  }
  if (incoming != localPCT){
    localPCT = incoming;
    _aStepper->setSpeedPct(localPCT);
#ifdef DEBUG_DUE_SLAVE    
    Serial.println("Received: " + String(localPCT) + "% from master.");
    showSpeed();
#endif
  }  
}

#ifdef DEBUG_DUE_SLAVE
void SlaveApp::showSpeed(){
  Serial.println("Current speed: " 
     + String(abs(_aStepper->getSpeedAsPct()))
     + "% " 
     + (_aStepper->getForward() ? "forward" : "reverse")); 
}
#endif
