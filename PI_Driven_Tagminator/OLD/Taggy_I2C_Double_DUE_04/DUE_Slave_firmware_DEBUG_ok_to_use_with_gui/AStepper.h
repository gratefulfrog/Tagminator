#ifndef ASTEPPER_H
#define ASTEPPER_H

#include <Arduino.h>
#include <AccelStepper.h>

#include "configI2C.h"

#define DEBUG_A_STEPPER

#define USE_DUE

#ifdef USE_DUE
#define MAX_STEP_SPEED   (30000)
#else
#define MAX_STEP_SPEED   (200)
#endif


class AStepper{
 protected:
  const  int _speedSign = -1;   // always
 
  bool _forward         = true,
       _enabled         = true;
  AccelStepper *_stepper;

 public:
  AStepper();
  void enable(bool yes);
  void setForward(bool);
  bool getForward() const;

  void setSpeedPct(signed char pct);
  signed char getSpeedAsPct() const;
  void step();
};

#endif
