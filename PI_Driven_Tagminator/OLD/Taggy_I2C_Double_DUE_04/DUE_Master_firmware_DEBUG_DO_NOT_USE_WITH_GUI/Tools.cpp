#include "Tools.h"

void delayMS(unsigned long del){
  unsigned long i = 0,
                now = millis();
  while(millis()-now< del){
    i++;  // this is here so the compiler won't optimize away this otherwise empty loop !
  }
}

char getChar(int i){
  return char(48+i);
}
