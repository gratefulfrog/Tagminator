#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

#define VERSION_NAME "Taggy_I2C_Double_DUE_05:DUE_Master_firmware and DUE_Slave_firmware"

// for Serial monitor interaction
#define SERIAL_BAUD_RATE   (115200)
  

/// DUE SLAVE I2C ADDRES  note: 0-7 are reserved addressses not to be used!
#define SLAVE_ADDRESS      (8)

/////// Define PINs here!
#define LABEL_SENSOR_PIN   (3) 
#define RELAY_PIN          (5)     
#define ENABLE_PIN         (6)  
#define DIR_PIN            (7)
#define MOTOR_PIN          (8)

/////// Set this according to the sensor, 
#define LABEL_DETECTED     (1)       // 0 if LOW when label detected, 1 if High

#endif
