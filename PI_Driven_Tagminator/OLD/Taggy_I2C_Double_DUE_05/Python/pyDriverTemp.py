import threading
import time
import random
#from functools import reduce

class PyDriverThread(threading.Thread):
    def __init__(self,outQ,inQ):
        """
        sends data to the gui
        """
        threading.Thread.__init__(self,daemon=True)
        self.outQ = outQ
        self.inQ = inQ
        self.enabled = False
        self.badCounter = self.counter  = 1
        self.limit = 0
        
    def sendToGui(self,msg):
        self.outQ.put(msg)

    def getSN(self,i,j):
        """ simulation makes by appending random 15 HEX digits to 'E'
        and sometimes generates a BAD_LABEL
        """
        
        res = ('BAD_LABEL_'+str(j),j+1)
        if i%5: # 1 in 5 is bad #random.randint(20,100):
            length = 15
            res = ('E' + ('{0:0'+str(length)+'x}').format(i-j).upper(),j)
        return res

    def processIncoming(self,item):
        print('PyDriver: read from inQ: ',item[0])
        com = int(item[0])
        arg = item[1:]
        if 0 == com:
            print('PyDriver: setting limit to: ' + arg)
            self.limit = int(arg)
        elif 1 == com or 2 == com:
            self.enabled = True
        elif 3 == com: # stop and reset
            self.enabled = False
            self.badCounter = self.counter  = 1
        elif 4 == com:
            self.enabled = False
        elif 5 == com:
            print('PyDriver: version 0')
        elif 6 == com:
            print('PyDriver: setting speed to ' + arg + '%')
            
    
    def run(self):
        """
        Thread run method. enqueues a message for the GUI
        """
        delay = 1 #ms
        now = time.time()
        while True:
            if self.enabled and (time.time()- now > delay) and self.counter<= self.limit:
                (outgoing,self.badCounter)=self.getSN(self.counter,self.badCounter)
                self.sendToGui([self.counter,outgoing])
                self.counter+=1
                now = time.time()
            self.enable = self.enabled and self.counter<= self.limit
            time.sleep(0.5)
            try:
                item = self.inQ.get(False)
            except Exception:
                pass
            else:
                self.processIncoming(item)
                self.inQ.task_done()
