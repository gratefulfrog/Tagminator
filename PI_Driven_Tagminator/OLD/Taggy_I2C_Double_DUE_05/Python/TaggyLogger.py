
from time import strftime
import re
import os

class Logger():
    filename = strftime('%Y_%m_%d_%H_%M.log')
    init     = False
    def __init__(self, mutex,name):
        self.mutex = mutex
        self.name = name
        if not Logger.init:
            with open(Logger.filename, 'a') as f:
                msg = 'Logging from: ' + os.getcwd()
                f.write(msg +'\n')
            Logger.init = True

    def sequenceTagIt(self,txt):
        result = txt
        target = '\\*.*\\n'
        res    = re.search(target,txt)
        if res != None: # then we got a ** something\n tag, reassemble
            s = res.start()
            e = res.end()
            result = txt[s:e] + txt[:s] + txt[e:]
        return result

        
    def logMessage(self,msg):
         self.mutex.acquire()
         try:
             rawLogText = self.name +': ' + msg
             logText = self.sequenceTagIt(rawLogText)
             print(logText)
             with open(Logger.filename, 'a') as f:
                 f.write(logText+'\n')
         finally:
             self.mutex.release()
    

