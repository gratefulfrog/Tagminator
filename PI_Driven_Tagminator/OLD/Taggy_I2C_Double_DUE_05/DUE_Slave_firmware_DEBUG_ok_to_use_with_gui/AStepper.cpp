#include "AStepper.h"


void AStepper::enable(bool yes){
  if (!yes){
    _stepper->setSpeed(0);
    _stepper->disableOutputs();
  }
  else{
    _stepper->enableOutputs();
  }
  _enabled = yes;

#ifdef DEBUG_A_STEPPER
  Serial.print("DUE Slave: Motors: ");
  Serial.println(yes ? "enabled." : "disabled.");
  Serial.println("DUE Slave: pin: " + String(ENABLE_PIN) + " : " + String(digitalRead(ENABLE_PIN) ? "HIGH" : "LOW"));
#endif
}

AStepper::AStepper(){
#ifdef USE_DUE
  pinMode(RELAY_PIN,OUTPUT);
  digitalWrite(RELAY_PIN,LOW); // LOW is off, ie REVERSE
  
  _stepper = new AccelStepper(AccelStepper::DRIVER ,MOTOR_PIN, DIR_PIN);   
  _stepper->setEnablePin(ENABLE_PIN);
  _stepper->setPinsInverted (true,  // directionInvert true means LOW is active,
                             true,  // stepInvert      true means LOW is active,
                             false); // enableInvert    true means LOW is active,
#else
  _stepper = new AccelStepper();  
#endif
  
  _stepper->setMaxSpeed(MAX_STEP_SPEED);
  _stepper->setSpeed(0);
  setForward(true);
  enable(false); 
}

void AStepper::setForward(bool yes){
  _forward = yes;
  if (yes) {        // FORWARD: we want motor 2 and negative speeds
    digitalWrite(RELAY_PIN, HIGH);
  }
  else{             // REVERSE: we want motor 1 and positive speeds 
    digitalWrite(RELAY_PIN, LOW);
  }
  delay(100);
#ifdef DEBUG_A_STEPPER
  Serial.println(String("DUE Slave: Selecting: ") + (_forward ? "forward" : "reverse"));  
  Serial.println(String("DUE Slave: Selecting: ") + (_forward ? "Motor 2" : "Motor 1"));  
#endif
}

bool AStepper::getForward() const{
  return _forward;
}

void AStepper::setSpeedPct(signed char pct){
  if (pct==0){
    _stepper->setSpeed(0);
    enable(false);
    return;
  }
  else{
    setForward(pct>=0);
  }
  float speedToSet = _speedSign*pct*MAX_STEP_SPEED*0.01 ; // pct is an integer percent!
  _stepper->setSpeed(speedToSet);
 #ifdef DEBUG_A_STEPPER
  Serial.println(String("DUE Slave: Setting speed: ") + String(speedToSet));
#endif 
  if(!_enabled){
    enable(true);
  }
}

signed char AStepper::getSpeedAsPct() const{
  float speedAsFloatingPct = 100.0*_speedSign*_stepper->speed()/MAX_STEP_SPEED;
 #ifdef DEBUG_A_STEPPER
  Serial.println(String("DUE Slave: speedAsFloatingPct: ") + String(speedAsFloatingPct));
#endif 
  signed char res = (signed char)round(speedAsFloatingPct );
  return res;
}

void AStepper::step(){
  _stepper->runSpeed();
}  
