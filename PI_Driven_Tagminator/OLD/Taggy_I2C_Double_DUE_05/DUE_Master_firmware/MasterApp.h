#ifndef MASTER_APP_H
#define MASTER_APP_H

//#define SERIAL2_DEBUG

//#define DEBUG_MASTER_APP

#include <Arduino.h>
#include "InputProcessor.h"
#include "LabelMgr.h"
#include "DueSlave.h"
#include "configI2C.h"
#include "Tools.h"

#define NB_APP_COMMANDS   (8)

class InputProcessor;

class MasterApp{
 public:
  // ISRs
  static volatile bool flag;
  static void onFall();
  static void onRise();
  
 protected:
  // Constants    
  static const String funcNameVec[],
                      _countReachedMsg;
  signed char _currentSpeedPct = 0;
  int         _count           = 0;
  int         _labelLimit      = 0;

  //long unsigned _startTime     = 0;
 
  // general control flags
  bool _stepping               = false;
 
  // member instances
  DueSlave     *_slave;
  LabelMgr     *_labelMgr;
    
  // input processor
  InputProcessor *ip;
  void _setLimit(unsigned lim);                                    // 0
  void _forward(unsigned unused=0);                                // 1   
  void _reverse(unsigned unused =0);                               // 2
  void _stop(unsigned unused =0);                                  // 3
  void _pause(unsigned unused=0);                                  // 4
  void _version(unsigned unused=0);                                // 5
  void _setSpeedPct(unsigned newSpeedPct);                         // 6
  void _debugShowHelp(unsigned unused=0);                          // 7

  void _enableStepping(bool yes,
		       int direction);  // direction 0: use current (correct for sign!),
                                        // direction 1: forward,
                                        // direction 2: reverse
  void _dof(String s);
    
  void    _initCounts();
  void    _initPins();
  void    _sendToServer(String msg); // terminated with \n
    
 public:
  MasterApp();
  //float _adjustSpeed(); // returns msPerLabel
  void mainLoop();
  void dof(unsigned short index,unsigned arg);    

#ifdef SERIAL2_DEBUG
  ////////////////// SERIAL2 DEBUGGING
 protected:
  void    _configSerial2Debug();
  void    _sendToDebug(String where);
#endif
};

typedef void (MasterApp::*appFprt)(unsigned arg);


#endif
