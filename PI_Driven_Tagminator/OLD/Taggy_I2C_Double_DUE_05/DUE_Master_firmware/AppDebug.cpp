#include "MasterApp.h"

#ifdef SERIAL2_DEBUG
void MasterApp::_configSerial2Debug(){
  Serial2.begin(115200);
}
void MasterApp::_sendToDebug(String where){
  String res = String("From\t: ")       + where;
         res += "\r\n_labelLimit\t: "     + String(_labelLimit);
         res += "\r\nLabel Count\t: "     + String(_count));
         res += "\r\n_stepping\t: "       + String((_stepping ? "True" : "False"));
         res += "\r\n";
   Serial2.print(res);
}    
#endif
