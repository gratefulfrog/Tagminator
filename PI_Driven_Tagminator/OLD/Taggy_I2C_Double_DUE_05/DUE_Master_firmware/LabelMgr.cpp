#include "LabelMgr.h"

LabelMgr::LabelMgr(){
  reset();
}
void LabelMgr::incCount(){
  _count++;
  unsigned long now = millis();
  _totalMS += now-_lastCountTime;
  _lastCountTime = now;
  
#ifdef LABEL_MRG_DEBUG
  Serial.println("LabelMgr count: " + String(_count));
#endif
}
unsigned long LabelMgr::getCount() const{
  return _count;
}
void LabelMgr::reset(){ // reset to zero everything
  _count = 0;
  _totalMS = 0;
  _lastCountTime = millis();
}
void LabelMgr::activate(){ // sets startTime
  _lastCountTime = millis();
}
float LabelMgr::msPerLabel() const{  // returns current millis per label since last activation
  float msPerLabel = (float)_totalMS/(float)_count;
#ifdef LABEL_MRG_DEBUG
  Serial.print("LabelMgr msPerLabel: ");
  Serial.println(msPerLabel); 
#endif  
  return msPerLabel;
}
