#include "InputProcessor.h"
 
 InputProcessor::InputProcessor(appFprt fVec[]){  // null terminated!
  int len = 0;
  while (fVec[len++]);
  funcPtrVec = new appFprt[--len];
  nbFuncs = len;
  for(short i =0;i<nbFuncs;i++){
    funcPtrVec[i]  = fVec[i];
  }
 }

 appFprt  InputProcessor::gett(unsigned short index){
  appFprt res = NULL;
  if (index < nbFuncs){
    res =  funcPtrVec[index];
  }
  return res;
}
