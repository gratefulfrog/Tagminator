#ifndef DUE_SLAVE_H
#define DUE_SLAVE_H

#include <Arduino.h>
#include <Wire.h>
#include "configI2C.h"

//#define DEFAULT_START_SPEED_PCT (30)

//#define DEBUG_DUE_SLAVE

class DueSlave{
 protected:
  signed char  currentSpeedPCt = 0;

 public:
  DueSlave();
  void        setSpeedPct(signed char pct);
  signed char getSpeedPct() const;
};
#endif
