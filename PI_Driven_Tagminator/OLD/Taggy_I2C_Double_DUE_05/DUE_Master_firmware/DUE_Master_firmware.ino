#include "configI2C.h"
#include "MasterApp.h"


MasterApp *masterApp;

void setup() {
  Serial.begin(SERIAL_BAUD_RATE);
  delayMS(200);
  masterApp = new MasterApp();
}

void loop(){
  masterApp->mainLoop();
}
