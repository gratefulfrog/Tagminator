#include "MasterApp.h"

volatile bool MasterApp::flag = false;
const String MasterApp::_countReachedMsg = String("0");

void MasterApp::onFall(){
  MasterApp::flag = false;
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),MasterApp::onRise,RISING);
}

void MasterApp::onRise(){
  MasterApp::flag = true;
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),MasterApp::onFall,FALLING);
}

const String MasterApp::funcNameVec[] = {"Set Limit: \'0n\' eg. 042 sets limit to 42",  // 0
                                         "Count Forward previoulsy set label limit",    // 1
                                         "Count Reverse previoulsy set label limit",    // 2
                                         "Stop and reset",                              // 3
                                         "Pause, do not reset counters",                // 4
                                         "Display firmaware version",                   // 5
					 "Set % Speed on [1,100]: \'6n'\ set speed to n percent",            // 6
                                         "Help"};                                       // 7

void MasterApp::_setLimit(unsigned lim){
  _labelLimit = lim;
#ifdef DEBUG_MASTER_APP
  Serial.println("Label Count limit set to : " + String(lim));
#endif
}

void MasterApp::_forward(unsigned unused){
  _enableStepping(true,1);
#ifdef DEBUG_MASTER_APP
  Serial.println("Counting forward : " + String(_labelLimit - _labelMgr->getCount()) + " labels.");
#endif
}
void MasterApp::_reverse(unsigned unused){
  _enableStepping(true,2);
#ifdef DEBUG_MASTER_APP
  Serial.println("Counting reverse : " + String(_labelLimit - _labelMgr->getCount()) + " labels.");
#endif
 }
void MasterApp::_stop(unsigned unused){
  _enableStepping(false,0);
  _initCounts();
  _sendToServer(_countReachedMsg);
#ifdef DEBUG_MASTER_APP
  Serial.println("Stopped. Label Limit: " + String(_labelLimit) + " Label count reset to : " + String(_labelMgr->getCount()));
#endif
}
void MasterApp::_pause(unsigned unused){
  _enableStepping(false,0);
#ifdef DEBUG_MASTER_APP
  Serial.println("Paused.  Label count:" + String(_labelMgr->getCount()) + "/" + String(_labelLimit));
#endif
}
void MasterApp::_version(unsigned unused){
  _sendToServer(VERSION_NAME);
}

void MasterApp::_setSpeedPct(unsigned newSpeedPct){
  if (_currentSpeedPct >0){  // then use absolute value
    _currentSpeedPct = (signed char)newSpeedPct;
  }
  else{  // negate
    _currentSpeedPct = -(signed char)newSpeedPct;
  }
  if(_stepping){
    _slave->setSpeedPct(_currentSpeedPct);
  }
#ifdef DEBUG_MASTER_APP
  Serial.println("Speed set to : " + String(_currentSpeedPct));
#endif
}

void MasterApp::_enableStepping(bool yes,
			        int direction){  // direction 0: use current (correct for sign!),
                                                 // direction 1: forward,
                                                 // direction 2: reverse
  signed char newSpeed  = 0;
  switch(direction){
    case 0:  // USE CURRENT
    newSpeed = _currentSpeedPct;
    break;
  case 1: // FORWARD
    newSpeed = abs(_currentSpeedPct);
    break;
  case 2:  // REVERSE
    newSpeed = -abs(_currentSpeedPct);
    break;
  }
  if (yes){
    _stepping = true;
    _slave->setSpeedPct(newSpeed);
    _currentSpeedPct = newSpeed;
    _labelMgr->activate();
  }
  else{
    _stepping = false;
    _slave->setSpeedPct(0);
  }
}

void MasterApp::_dof(String s){
  unsigned short comIndex = s.substring(0,1).toInt();
  unsigned   comArg   = s.substring(1).toInt();

#ifdef SERIAL2_DEBUG
  ////////////////////////////  SERIAL2 DEBUGGING /////////////////:
  _sendToDebug(String("at top of dof, args:\t: ") + String(comIndex) + ", " + String(comArg));
#endif

  appFprt fp = ip->gett(comIndex);
  if (fp){
    (this->*fp)(comArg);
  }
#ifdef SERIAL2_DEBUG
   ////////////////////////////  SERIAL2 DEBUGGING /////////////////:
   _sendToDebug("Exiting _dof");
#endif
}

void MasterApp::_initCounts(){
  _labelMgr->reset();
  //_currentSpeedPct = DEFAULT_START_SPEED_PCT;
}

void MasterApp::_sendToServer(String msg){ // terminated with \n
  Serial.print(msg);
  Serial.print('\n');
}
 
MasterApp::MasterApp(){
  _slave = new DueSlave();
  _labelMgr = new LabelMgr();
  
  appFprt vec[]  = {
    &MasterApp::_setLimit,      // 0
    &MasterApp::_forward,       // 1
    &MasterApp::_reverse,       // 2
    &MasterApp::_stop,          // 3
    &MasterApp::_pause,         // 4
    &MasterApp::_version,       // 5
    &MasterApp::_setSpeedPct,   // 6
    
#ifdef DEBUG_MASTER_APP
    &MasterApp::_debugShowHelp, // 7
#endif
    NULL};
    
  ip = new InputProcessor(vec);
  pinMode(LABEL_SENSOR_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),onRise,RISING);

//////////////////// DEBUGGING ////////////////////////:
#ifdef SERIAL2_DEBUG
  _configSerial2Debug();
  _sendToDebug("End of constructor");
#endif

#ifdef DEBUG_MASTER_APP
  Serial.println("Ready!");
  int startDelay = 5;
  while(startDelay){
    Serial.print("Starting in ");
    Serial.print(startDelay);
    Serial.println(String(" second") + (startDelay-- != 1 ? "s" : ""));
    delayMS(1000);
  }
  _debugShowHelp();
#endif
}

void MasterApp::mainLoop(){
  static bool lastFlag = false;

  bool localFlag = flag;

  if (Serial.available()){
    _dof(Serial.readString());
  }

  if(!_stepping){
      return;
  }

  signed char newSpeed = _slave->getSpeedPct();
  if(lastFlag && !localFlag){
    _labelMgr->incCount();
    _sendToServer(String(_labelMgr->getCount()));
    //float msPerLabel = _adjustSpeed(); 

#ifdef DEBUG_MASTER_APP
    static long unsigned lastTiming = _labelMgr->msPerLabel();
    if (_labelMgr->getCount()){
      Serial.print("GAP after label: "); 
      Serial.print(_labelMgr->getCount());
      Serial.print("  msPerLabel: " );
      Serial.print(_labelMgr->msPerLabel()); 
      Serial.print(" ms per label ");
      Serial.print("  delta msPerLabel: " );
      Serial.print(_labelMgr->msPerLabel() - lastTiming);
      lastTiming = _labelMgr->msPerLabel();
      Serial.print(" ms per label.     New target speed percent: ");
      Serial.print(_currentSpeedPct);
      Serial.println("%");
    }
#endif
  }
  lastFlag = localFlag;
  
  if (_labelMgr->getCount() == _labelLimit){
#ifdef DEBUG_MASTER_APP
    Serial.println("Count reached : " + String(_labelMgr->getCount()) + " stopped!");
#endif
    _stop();
  }
  else{ // step
    if(!_stepping){
      _enableStepping(true,0);
    }
  }
}

#ifdef DEBUG_MASTER_APP
void MasterApp::_debugShowHelp(unsigned unused_1){
  Serial.println("Enter commands by typing:"); 
  for (int i = 0;i<NB_APP_COMMANDS;i++){
    Serial.println(String(i) + " : " + String(funcNameVec[i]));
  }
}
#endif
