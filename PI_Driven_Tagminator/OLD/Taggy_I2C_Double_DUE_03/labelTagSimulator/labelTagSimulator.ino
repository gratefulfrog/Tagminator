#define LABEL_SENSOR_PIN (2)    
#define CONTROL_PIN      (3)
#define LABEL_DETECTED   (1)

#define BAUD (9600)

#define GAP_TIME       (50)
#define LABEL_GAP_TIME_FACTOR (25)
#define DETECTION_TIME (GAP_TIME*LABEL_GAP_TIME_FACTOR)

#define ANALOG_INPUT_PIN  (A0)

//#define DEBUG_LABEL_TAG_SIMULATOR

typedef void (*fptr)();

int gapTime = GAP_TIME,
  detectionTime = DETECTION_TIME;

char hexVec[] = {'0', '1', '2', '3',
                 '4', '5', '6', '7',
                 '8', '9', 'A', 'B',
                 'C', 'D', 'E', 'F'};
                  
void doPins(int v){
  digitalWrite(LED_BUILTIN,v);
  digitalWrite(LABEL_SENSOR_PIN,v);
}

String getSequentialSN(){
  static long unsigned snID = 0;
  char buff[18];
  sprintf(buff,"E%016u",snID++);
  return String(buff);
}


String getHexSN(){
  static long unsigned lastTime = millis();
  long unsigned now = millis(),
                elapsedTime = now - lastTime;
  lastTime = now;
  
  String res = String("E");
  for (int i=0;i<16;i++){
    res += String(hexVec[random(0,16)]) ;
  }
#ifdef DEBUG_LABEL_TAG_SIMULATOR  
  res += String("  :  time: ");
  res += String(now);
  res += String("  :  elapsed time: ");
  res += String(elapsedTime);
#endif
  return res;
}

void good(bool yes){
  doPins(LABEL_DETECTED);  
  if (yes){
    //Serial.println(getHexSN());
    Serial.println(getSequentialSN());
  }
  else{
#ifdef DEBUG_LABEL_TAG_SIMULATOR
    Serial.println("bad");
#endif
  }
  delay(detectionTime);  
  doPins(!LABEL_DETECTED);  
  delay(gapTime);
}

#ifdef DEBUG_LABEL_TAG_SIMULATOR
void bad(){
  Serial.println("Bad Label");
  delay(detectionTime);
  doPins(!LABEL_DETECTED);
}
#endif

void setup() {
  pinMode(LABEL_SENSOR_PIN, OUTPUT);
  pinMode(CONTROL_PIN,INPUT_PULLUP);
  digitalWrite(LABEL_SENSOR_PIN, LOW);
  Serial.begin(BAUD);
  //while(!Serial);
  //Serial.println("Starting up!");
}

bool goodToGo(){
  return (!digitalRead(CONTROL_PIN));
}

void adjustTiming(){
  // read A0 and reset the delay
  static const int maxGapTime = 100,
    minGapTime = 10;  // millis

  static long unsigned lastChangeTime = millis();

  if (millis()-lastChangeTime < 2000){
    return;
  }
  lastChangeTime = millis();
  
  unsigned int reading = analogRead(ANALOG_INPUT_PIN);
  gapTime = map(reading,0,1023,minGapTime,maxGapTime); 
  detectionTime = LABEL_GAP_TIME_FACTOR * gapTime;
#ifdef DEBUG_LABEL_TAG_SIMULATOR
  Serial.println("Gap time: " + String(gapTime));
  Serial.println("Label time: " + String(detectionTime));
#endif
}


  
void loop(){
  static bool yes = true;
  if (goodToGo()){
    good(yes);
    yes = !yes;
  }
  //adjustTiming();
}
