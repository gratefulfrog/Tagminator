#include "DueSlave.h"


DueSlave::DueSlave(){
  Wire.begin(); // join i2c bus (address optional for master)
  delay(500);
  setSpeedPct(0);
}

void DueSlave::setSpeedPct(signed char pct){
  currentSpeedPCt  = pct;
  Wire.beginTransmission(SLAVE_ADDRESS); 
  Wire.write(pct);        
  Wire.endTransmission();  
  #ifdef DEBUG_DUE_SLAVE
  Serial.println("DueSlave::setSpeedPct " +String(pct) + " sent to IC2");
  #endif
}

signed char DueSlave::getSpeedPct() const{
  return currentSpeedPCt;
}
