#include "configI2C.h"
#include "MasterApp.h"


MasterApp *masterApp;

void setup() {
  Serial.begin(SERIAL_BAUD_RATE);
  //while(!Serial);
  delayMS(100);
  //establishContact();
  delayMS(100);
  masterApp = new MasterApp();
}

void loop(){
  masterApp->mainLoop();
}

void establishContact() {
  while (Serial.available() <= 0) {
    Serial.println("A");   // send a capital A
    delay(300);
  }
  Serial.read();
}
