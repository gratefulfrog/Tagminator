#!/usr/bin/python3

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import csv

import types
import queue
import threading
import time

from TaggyLogger import Logger

versionString = 'Taggy2020_007'

badLabelString = 'BAD_LABEL'

activeColor     = 'blue'
forwardColor    = 'green'
reverseColor    = 'cyan4'
entryColor      = 'white'
limitColor      = 'SeaGreen1'
limitActiveColor= 'cornsilk3'
limitErrorColor = 'red'
stopColor       = 'red'
pauseColor      = 'yellow'
listColor       = 'gray80'
badTagColor     = 'red'
doubleSNColor   = 'orange2'
saveColor       = 'cornsilk3'
quitColor       = 'cornsilk4'
countColor      = ['sea green',       # 0
                   'PaleGreen4',      # 1
                   'PaleGreen3',      # 2
                   'PaleGreen2',      # 3
                   'PaleGreen1',      # 4
                   'goldenrod1',      # 5
                   'goldenrod2',      # 6
                   'goldenrod3',      # 7
                   'goldenrod4',      # 8
                   'firebrick1',      # 9
                   'firebrick2',      # 10
                   'firebrick3',      # 11
                   'firebrick4',      # 12
                   'orange red']      # 13

def getCountColor(nbGood=0,nbBad=0):
    if nbBad == 0:
        return countColor[0]
    else:
        failurePct = min(len(countColor)-1,round(100*nbBad/(nbGood+nbBad)))
        return countColor[failurePct]


### simulation variables
listBoxPollDelay = 100 # milliseconds
maxSNLength  = 30
maxQWait     = 5 # seconds

class TaggyGui(tk.Frame,Logger):
    commandDict = {'l' : '0', # followed by argument as a string!
                   'f' : '1',
                   'r' : '2',
                   's' : '3',
                   'p' : '4',
                   'v' :  None,  # not implemented
                   'h' :  None}  # not implemented
    def __init__(self,mutex,inQ,outQ,master=None):
        tk.Frame.__init__(self, master)
        Logger.__init__(self,mutex,'GUI')
        self.outQ   = outQ
        self.inQ    =  inQ
        self.master.title("Tagminator")
        self.master.geometry('1000x800')                
        self.grid()
        self.currentLimit = tk.IntVar()
        self.countString  = tk.StringVar()
        self.widgetDict = {}
        
        limitEntryArgDict = {  'widget'  : 'tk.Entry',
                               'argDict' : {'master'       : self.master,
                                            'name'         : 'limitEntry',
                                            'textvariable' : self.currentLimit,
                                            'bg'           : limitColor,
                                            'justify'      : tk.CENTER,
                                            'relief'       : 'sunken'},
                               'gridDict' : {'row'    : 0,
                                             'column' : 0,
                                             'sticky' : 'NSEW'},
                               'confFunc' : self.configureLimit}

        limLabelArgDict = {  'widget': 'tk.Label',
                             'argDict' : {'master' : self.master,
                                          'name'   : 'limitLabel',
                                          'text'   : 'Label Limit',
                                          'bg'     : limitColor,
                                          'relief': 'sunken'},
                             'gridDict' : {'row'    : 1,
                                           'column' : 0,
                                           'sticky' : 'NSEW'}}
        
        forwardButtonArgDict = {  'widget': 'tk.Button',
                                  'argDict' : {'master' : self.master,
                                               'name'   : 'forwardButton',
                                               'text'   : 'Forward',
                                               'bg'     : forwardColor,
                                               'activebackground' : activeColor,                       
                                               'command'          : lambda : self.directionCallBack(0)},
                                  'gridDict' : {'row'    : 0,
                                                'column' : 1,
                                                'sticky' : 'NSEW'}} 

        reverseButtonArgDict = {  'widget': 'tk.Button',
                                  'argDict' : {'master' : self.master,
                                               'name'   : 'reverseButton',
                                               'text'   :  'Reverse',
                                               'bg'     : reverseColor,
                                               'activebackground' : activeColor,
                                               'command'          : lambda : self.directionCallBack(1)},
                                  'gridDict' : {'row'    : 1,
                                                'column' : 1,
                                                'sticky' : 'NSEW'}}

        stopButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : self.master,
                                          'name'   : 'stopButton',
                                          'text'   : 'Stopped',
                                          'bg'     : activeColor,
                                          'activebackground' : activeColor,
                                          'command' : self.stopCallBack},
                             'gridDict' : {'row'    : 0,
                                           'column' : 2,
                                           'sticky' : 'NSEW'}}
        
        pauseButtonArgDict = {'name' : 'pauseButton',
                              'widget': 'tk.Button',
                              'argDict' : {'master'  : self.master,
                                           'name'    : 'pauseButton',
                                           'text'    : 'Pause',
                                           'bg'      : pauseColor,
                                           'activebackground' : activeColor,
                                           'command' : self.pauseCallBack},
                              'gridDict' : {'row'    : 1,
                                            'column' : 2,
                                            'sticky' : 'NSEW'}}

        countLabelArgDict = {  'widget': 'tk.Label',
                               'argDict' : {'master'       : self.master,
                                            'name'         : 'countLabel',
                                            'textvariable' : self.countString,
                                            'bg'           : getCountColor(),
                                            'justify'      : tk.CENTER,
                                            'relief'       : 'sunken'},
                               'gridDict' : {'row'    : 2,
                                             'column' : 0,
                                             'columnspan' : 3,
                                             'sticky' : 'NSEW'}}


        listFrameArgDict = { 'widget': 'tk.Frame',
                             'argDict' : {'master'  : self.master,
                                          'name'    : 'listFrame',
                                          'bd'      : 1,
                                          'relief'  : 'sunken'},
                             'gridDict' : {'row'        : 3,
                                           'column'     : 0,
                                           'columnspan' : 3,
                                           'sticky'     : 'NSEW'}}
        
        scrollbarArgDict = { 'widget': 'tk.Scrollbar',
                             'argDict' : {'master' :  lambda : self.widgetDict['listFrame'],
                                          'name'    : 'scrollbar',
                                          'orient'  : tk.VERTICAL},
                             'gridDict' : {'row'        : 0,
                                           'column'     : 1,
                                           'sticky'     : 'NSEW'}}
        
        listboxArgDict = { 'widget': 'tk.Listbox',
                           'argDict' : {'master' :  lambda : self.widgetDict['listFrame'],
                                        'name'    : 'listbox',
                                        'bd'      : 0,
                                        'justify' : tk.LEFT,
                                        'font'    : 'mono'},
                           'gridDict' : {'row'        : 0,
                                         'column'     : 0,
                                         'sticky'     : 'NSEW'}}
        
        saveFrameArgDict = { 'widget': 'tk.Frame',
                             'argDict' : {'master'  : self.master,
                                          'name'    : 'saveFrame',
                                          'bd'      : 1,
                                          'relief'  : 'sunken'},
                             'gridDict' : {'row'        : 4,
                                           'column'     : 0,
                                           'columnspan' : 3,
                                           'sticky'     : 'NSEW'}}
        
        saveButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : lambda : self.widgetDict['saveFrame'],
                                          'name'   : 'saveButton',
                                          'text'   : 'Save Data File',
                                          'bg'     :  saveColor,
                                          'activebackground' : activeColor,
                                          'command'          : lambda : self.saveFile()},
                             'gridDict' : {'row'    : 0,
                                           'column' : 0,
                                           'sticky' : 'NSEW'}}

        quitButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : lambda : self.widgetDict['saveFrame'],
                                          'name'   : 'quitButton',
                                          'text'   : 'Quit',
                                          'bg'     :  quitColor,
                                          'activebackground' : activeColor,
                                          'command'          : self.quit},
                             'gridDict' : {'row'    : 0,
                                           'column' : 1,
                                           'sticky' : 'NSEW'}}
        widgetSpecs = [limitEntryArgDict,
                       limLabelArgDict,
                       stopButtonArgDict,
                       pauseButtonArgDict,
                       forwardButtonArgDict,
                       reverseButtonArgDict,
                       countLabelArgDict,
                       listFrameArgDict,
                       scrollbarArgDict,
                       listboxArgDict,
                       saveFrameArgDict,
                       saveButtonArgDict,
                       quitButtonArgDict]
        
        self.createWidgets(widgetSpecs)
        self.doBindings()
        self.finalizeFrames()
        self.setupScrollBar()
        self.goodCount = 0
        self.badCount  = 0
        self.errorCount = 0
        self.updateCountLabel()
        self.afterID = None
        self.startPolling()
        self.logMessage('polling started in constructor: ' +str(self.afterID))
        
    def  createWidgets(self,widgetSpecs):
        for dict in widgetSpecs:
            try:
                dict['argDict']['master'] = dict['argDict']['master']()
            except:
                pass
            self.widgetDict[dict['argDict']['name']] = eval(dict['widget'])( **dict['argDict'])
            self.widgetDict[dict['argDict']['name']].grid(**dict['gridDict'])
            try:
                dict['confFunc']()
            except:
                pass
                
    def doBindings(self):
        self.master.bind('<Alt-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-x>',     lambda *args : self.quit() )
        self.master.bind('<Control-q>', lambda *args : self.quit() )
        self.widgetDict['countLabel'].bind( "<Button>", lambda *args :  self.resetCount()) 
    def finalizeFrames(self):
        colWeights = [1,6,5]
        rowWeights = [2,2,1,20,2]
        nbCols = len(colWeights)
        nbRows = len(rowWeights)
        
        self.widgetDict['listFrame'].columnconfigure(0,weight=1)
        self.widgetDict['listFrame'].rowconfigure(0,weight=1)

        self.widgetDict['saveFrame'].rowconfigure(0, weight=1)
        self.widgetDict['saveFrame'].columnconfigure(0,weight=1)
        self.widgetDict['saveFrame'].columnconfigure(1,weight=1)

        for x in range(nbCols):
            self.master.columnconfigure( x, weight=colWeights[x])
        
        for y in range(nbRows):
            self.master.rowconfigure(y, weight=rowWeights[y])

    def setupScrollBar(self):
        self.widgetDict['scrollbar'].configure(command=self.widgetDict['listbox'].yview)
        self.widgetDict['listbox'].configure(yscrollcommand=self.widgetDict['scrollbar'].set)

    ##################### VERSION ####################################
            
    def showVersion(self,*args):
        messagebox.showinfo('System Version: ', versionString)

    ################## Direction #####

    def directionCallBack(self,direction=0):
        dButVec = [[self.widgetDict['forwardButton'],'f','forwardColor','Forward'],
                   [self.widgetDict['reverseButton'],'r','reverseColor','Reverse']]
        triplet = dButVec[direction]
        command = triplet[1]
        commandText = triplet[3] 
        bText   = (commandText  if not direction else commandText[:-1]) + 'ing'
        if not self.isMoving():
            triplet[0].configure(text=bText,bg=activeColor)
            self.resetStopPause()
            self.clearInQ()
            self.sendCommand(command)

    def resetDirText(self ):
        self.widgetDict['forwardButton'].configure(text='Forward',bg=forwardColor)
        self.widgetDict['reverseButton'].configure(text='Reverse',bg=reverseColor)
        #print('GUI: polling stopeed in resetDirText ', self.afterID)
        #self.stopPolling()

    def isMoving(self):
        return 'ing' in self.widgetDict['forwardButton'].cget('text') + \
            self.widgetDict['reverseButton'].cget('text')
        
    ######################## stop pause #####
        
    def stopCallBack(self,nop=False):
        stopButton = self.widgetDict['stopButton']
        pauseButton = self.widgetDict['pauseButton']
        if stopButton.cget('text') == 'Stop':
            if not nop:
                self.sendCommand('s')
            stopButton.configure(text='Stopped',bg=activeColor)
            pauseButton.configure(text='Pause',bg=pauseColor)
            self.resetDirText()
            
    def pauseCallBack(self):
        stopButton = self.widgetDict['stopButton']
        pauseButton = self.widgetDict['pauseButton']
        if pauseButton.cget('text') == 'Pause' and \
           stopButton.cget('text') != 'Stopped':
            self.sendCommand('p') #pauseButton.cget('text'))
            stopButton.configure(text='Stop',bg=stopColor)
            pauseButton.configure(text='Paused',bg=activeColor)
            self.resetDirText()

    def resetStopPause(self):
        self.widgetDict['pauseButton'].configure(text='Pause',bg='yellow')
        self.widgetDict['stopButton'].configure(text='Stop',bg='red')

    ######################### Label Limit  ############""

    def configureLimit(self):
        self.currentLimit.set(0)
        self.currentLimit.trace(mode ='w',
                                callback=self.testLim)
        self.widgetDict['limitEntry'].bind('<Return>', self.setLim )
            
    def testLim(self,*args):
        e = self.widgetDict['limitEntry']
        try:
            self.sendCommand('l',str(currentLimit.get()))
            #e.configure(bg = entryColor)
            e.configure(bg = limitActiveColor)
        except:
            e.configure(bg = limitErrorColor)

    def setLim(self,*args):
        e = self.widgetDict['limitEntry']
        l = self.widgetDict['limitLabel']
        #print(int(str(self.currentLimit.get())))
        self.currentLimit.set(int(str(self.currentLimit.get())))
        try:   
            self.sendCommand('l',str(self.currentLimit.get()))
            e.configure(bg = limitColor)
            l.configure(bg = limitColor)
        except:
            e.configure(bg = limitErrorColor)
            l.configure(bg = limitErrorColor)

            
    ######################### countLabel #####################################

    def resetCount(self):
        self.goodCount  = 0
        self.badCount   = 0
        self.errorCount = 0
        self.restCounts = False 
        self.updateCountLabel()

    def updateCountLabel(self):
        #formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength) +'}'
        formatString = 'Label Count: {:>5d}  :  Good Labels: {:>5d}  :  Bad Labels: {:>5d}  :  Errors: {:>5d}'
        self.countString.set(formatString.format(self.goodCount+self.badCount,
                                                 self.goodCount,
                                                 self.badCount,
                                                 self.errorCount))
        self.widgetDict['countLabel'].configure(bg=getCountColor(self.goodCount,
                                                                 self.badCount))

        
    
    ######################### listbox #####################################

    def getFromQ(self):
        #print('GUI: polling from the gui...')
        formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength*2) +'}' 
        try:
            item = self.inQ.get(False)
            self.inQ.task_done()
        except queue.Empty:
            item = None
        if item:
            if int(item[0]) == 0:
                #reached limit
                self.stopCallBack(nop=True) # automatic stop
            else:
                debugMsg = '** GUI Display Label\n* got this from the inQ: <start>' +str(item) +'<end>' 
                self.logMessage(debugMsg)
                if self.isMoving():
                    listbox = self.widgetDict['listbox']
                    listbox.insert(tk.END,  formatString.format(int(item[0]),str(item[1])))
                    self.goodOrBadLabelLoad(listbox,item)
                    listbox.yview(tk.END)
                else:
                    self.logMessage('Taggy not running, so inQ item is rejected!')
        self.startPolling()
            #print('GUI: polling restarted in getFromQ ', self.afterID)
        

    def goodOrBadLabelLoad(self,listbox,item):
        col = listColor
        if badLabelString in item:  # it's a bad boy
            col = badTagColor
            self.badCount +=1
        elif '-1' in item[0:6]:  # double SN
            col = doubleSNColor
            self.errorCount +=1
        else:
            self.goodCount += 1
        listbox.itemconfig(tk.END, background = col)
        self.updateCountLabel()

    def startPolling(self):
        self.afterID = self.after(listBoxPollDelay,self.getFromQ)

    def stopPolling(self):
        self.logMessage('polling canceled on ' + str(self.afterID))
        self.after_cancel(self.afterID)

        
    ############################### SAVE FILE ################################

    def saveFile(self):
        data = [('All tyes(*.*)', '*.*')]
        file = filedialog.asksaveasfile(mode='w', defaultextension=".csv")
        if file:
            headers = ('Sequence','Serial_Number')
            writer = csv.writer(file, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(headers)
            for line in self.widgetDict['listbox'].get(0,tk.END):
                lineSplit = line.strip().split(' ')
                #print([lineSplit[0],lineSplit[-1]])
                writer.writerow([int(lineSplit[0]),lineSplit[-1]])
            file.close()
            messagebox.showinfo('Created File: ',file.name)

    ############################# SEND COMMAND ###############################
    
            
    def sendCommand(self,com, arg = None):
        #print('GUI: Command : ', com, arg)
        outgoing = ''
        if com:
            outgoing = TaggyGui.commandDict[com]
            if arg != None:
                outgoing += arg
        if outgoing:
            debugMsg = '** GUI command: <start>'+ com + '<end>'
            debugMsg += ' argument: <start>' + arg + '<end>\n' if arg != None else '\n'
            debugMsg += 'Sending command: <start>' + outgoing + '<end>'
            self.logMessage(debugMsg)
            self.outQ.put(outgoing)

    def clearInQ(self):
        good = True
        while good:
            try:
                item = self.inQ.get(False)
                self.inQ.task_done()
                self.logMessage('Q cleared of this: '+ str(item))
                time.sleep(0.1)
            except queue.Empty:
                good = False

def runIt():
    from pyDriverTemp import PyDriverThread as pyDriver
    import queue

    pyDriverToGuiQ = queue.Queue()
    guiToPyDriverQ = queue.Queue()

    pDT = pyDriver(pyDriverToGuiQ)
    pDT.start()

    app = TaggyGui(pyDriverToGuiQ, guiToPyDriverQ)
    app.mainloop()


if __name__ == '__main__':
    runIt()
