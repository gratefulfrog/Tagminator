#!/usr/bin/python3

from time import sleep
import threading
import sys

class Thready(threading.Thread):
    def __init__(self, startWorkEvent,quitEvt, isMaster = None):
        threading.Thread.__init__(self,daemon=True)
        if isMaster:
            self.run = self.masterRun
        else:
            self.run = self.slaveRun
        self.okToWork = startWorkEvent
        self.stop = quitEvt
        self.count = 0

    def masterRun(self):
        while True:
            try:
                if self.stop.is_set():
                    print('\nMaster exiting...')
                    return
                sleep(1)
                if self.okToWork.is_set():
                    self.okToWork.clear()
                else:
                    self.okToWork.set()
            except Exception as e:
                print(e)
                print('\n* Master thread exiting on exception...')
            
    def slaveRun(self):
        while True:
            try:
                if self.stop.is_set():
                    print('\nSlave exiting...')
                    return
                self.work(self.okToWork.is_set())
            except Exception as e:
                print(e)
                print('\n* Slave thread exiting on exception...')

    def work(self,ok):
        if ok:
            print('working:', self.count)
        else:
            print('rejecting:', self.count)
        self.count+=1
            

                          
def startup():
    quit = threading.Event()
    quit.clear()
    workController = threading.Event()
    workController.clear()
    slave  = Thready(workController,quit)
    master = Thready(workController,quit,True)
    
    slave.start()
    master.start()
    try:
        while(True):
            pass
    except:
        quit.set()
        master.join()
        slave.join()
        
        print('\nBye!')
        sys.exit(0)


if __name__ == '__main__':
    startup()


