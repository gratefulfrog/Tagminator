#!/usr/bin/python3

import subprocess

def getRaw():
    res = subprocess.run(['./tools/ttyScan.bash'],stdout=subprocess.PIPE)
    lis = res.stdout.decode('utf-8').split('\n')
    lol = []
    for l in lis:
        lol.append(l.split(' - '))
        
    return lol

def findS(s,lol):
    res = []
    for pair in lol:
        try:
            if s in pair[1]:
                res.append(pair[0])
            else:
                pass
        except:
            pass
    return res
    



dueNameLis      = ['Due',
                   'Mega']  # Meg is for Bob's simulation
tagReaderNameLis = ['STMicroelectronics',
                    'Scemtec',
                    '0043'] # 0043 is for Bob's simulation 


def findPort(pLis):
    for p in pLis:
        if p:
            return p[0]


def getDuePort():
    pLis = [findS(s,getRaw())  for s in dueNameLis]
    return findPort(pLis)

def getTagReaderPort():
    pLis = [findS(s,getRaw())  for s in tagReaderNameLis]
    return findPort(pLis)

if __name__ == '__main__':
    print(getDuePort())
    print(getTagReaderPort())
    print('if not found, this is returned: ', findPort([]))

