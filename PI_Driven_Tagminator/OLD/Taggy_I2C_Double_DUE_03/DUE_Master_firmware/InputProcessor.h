#ifndef INPUTPROCESSOR_H
#define INPUTPROCESSOR_H

#include "MasterApp.h"

class MasterApp;
typedef void (MasterApp::*appFprt)(unsigned arg);

class InputProcessor{
  private:
    appFprt *funcPtrVec;
    unsigned short nbFuncs;
    
  public:
    InputProcessor(appFprt[]);  // null terminated!
    appFprt gett(unsigned short index);
};

#endif
