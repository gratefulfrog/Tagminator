#ifndef LABEL_MGR_h
#define LABEL_MGR_h

#include <Arduino.h>

//#define LABEL_MRG_DEBUG

class LabelMgr{
  protected:
    unsigned int _count = 0;
    unsigned long _totalMS = 0,
                  _lastCountTime;

  public:
    LabelMgr();
    void incCount();
    unsigned long getCount() const;
    void reset(); // reset to zero everything
    void activate(); // sets startTime
    float msPerLabel() const;  // returns current millis per label since last activation
};


#endif
