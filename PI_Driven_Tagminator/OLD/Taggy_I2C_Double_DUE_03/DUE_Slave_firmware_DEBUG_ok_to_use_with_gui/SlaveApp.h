#ifndef SLAVE_APP_H
#define SLAVE_APP_H

#include <Arduino.h>
#include <Wire.h>
#include "AStepper.h"

#include "configI2C.h"

#define DEBUG_DUE_SLAVE

class SlaveApp{
  protected:
    AStepper *_aStepper;
    volatile signed char incoming = 0;
  public:
    static void receiveEvent(int);
    static SlaveApp *thisPtr;
    
    SlaveApp();
    void mainLoop();

#ifdef DEBUG_DUE_SLAVE
  protected:
    void showSpeed();
#endif
};

#endif
