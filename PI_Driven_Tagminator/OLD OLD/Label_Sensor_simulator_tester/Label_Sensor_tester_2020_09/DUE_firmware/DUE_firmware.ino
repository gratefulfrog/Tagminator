#include "config.h"

volatile bool flag = false;

void onRise(){
  flag = true;
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),onFall,FALLING);
}
void onFall(){
  flag = false;
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),onRise,RISING);
}


void setup() {
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  delay(100);
  pinMode(LABEL_SENSOR_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN),onRise,RISING);
  Serial.println("Ready!");
}

void loop(){
  static bool lastFlag = false;
  bool localFlag = flag;
  static unsigned long    count = 0;
  
  if(lastFlag && !localFlag){
    ++count;
  }
  Serial.println(localFlag ? "LABEL: " + String(count) : "GAP");
  lastFlag = localFlag;
}
