#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

// remove comments on next line to run automatic tests
//#define DEBUG
//#define MOTOR_SPEED_DEBUG

#define VERSION_NAME "Label_Sensor_tester_2020_09/DUE_firmware"

// for Serial monitor interaction
#define SERIAL_BAUD_RATE   (115200)

/////// Define PINs here!
#define LABEL_SENSOR_PIN   (3) 
#define RELAY_PIN          (5)     // SET to FORWARD_DIRECTION below for forward direction     
#define ENABLE_PIN         (6)  
#define DIR_PIN            (7)
#define MOTOR_PIN          (8)

/////// Motor Speed and Direction settings
#define FORWARD_DIRECTION      (0)       // set this to 0 if LOW makes the motor go forward!
#define INVERT_RELAY_DIRECTION (1)       // set this to 0 if the relay direction is the same as the motor direction
#define SPEED_DELAY            (150)
#define MOTOR_DELAY            (4)
#define MIN_SPEED_DELAY        (75)
#define MIN_MOTOR_DELAY        (2)

//#define LABEL_DETECTOR_BOUNCE_DELAY (50) // milliseconds

/////// Set this according to the sensor, 
#define LABEL_DETECTED     (1)       // 0 if LOW when label detected, 1 if High

/////////////// INTERNALS DO NOT MODIFY - and you know who ou are !!! ////////////////
#define  ADJUST_SPEED_EVERY    (20)     // nulmber of labels to detect before speed change
#define  ADJUST_MOTOR_SPEED    false    // disabled for the moment

#endif
