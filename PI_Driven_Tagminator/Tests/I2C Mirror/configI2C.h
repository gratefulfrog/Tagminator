#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

#define VERSION_NAME "Tests:I2C_Master_tester"

// for Serial monitor interaction
#define SERIAL_BAUD_RATE   (115200)
  
/// DUE SLAVE I2C ADDRES  note: 0-7 are reserved addressses not to be used!
#define SLAVE_ADDRESS       (9)
#define MASTER_ADDRESS      (8)

#endif
