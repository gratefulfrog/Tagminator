#ifndef MASTER_APP_H
#define MASTER_APP_H

//#define SERIAL2_DEBUG

//#define DEBUG_MASTER_APP

#include <Arduino.h>
#include <Wire.h>

#include "configI2C.h"
class MasterApp{
 public:
  
 protected:
  // Constants    
 public:
  MasterApp();
  void mainLoop();
};

#endif
