#include "MasterApp.h"


 
MasterApp::MasterApp(){
  Wire.begin(MASTER_ADDRESS);
  Serial.begin(SERIAL_BAUD_RATE);
  delay(10);
  Serial.println("Master: running...");
}

void MasterApp::mainLoop(){
  int len = -1;
    if(Serial.available()>0){
      String incoming = Serial.readString();
      Serial.println("Received from Serial: [" + incoming + "]");
      Serial.println("Writing to I2C...");
      Wire.beginTransmission(SLAVE_ADDRESS);  // Transmit to device 
      const char* cStringPtr = incoming.c_str();
      Wire.write(cStringPtr);             // Sends value byte
      Wire.endTransmission();      // Stop transmitting
      delay(10);
      Wire.requestFrom(SLAVE_ADDRESS, incoming.length());
      len = incoming.length();
    }
    int wav = Wire.available();
    if(wav==len) {
      Serial.println("Start of Wire reception: " + String(wav) + " bytes.");
      String rS;
      while(Wire.available()) {
          rS += char(Wire.read());    // Receive a byte as character anbd add to String
      }
      Serial.print("Received from wire: ");
      Serial.println( '[' + rS + ']');         // Print the string
      Serial.println("End of Wire reception.");
    }
}
