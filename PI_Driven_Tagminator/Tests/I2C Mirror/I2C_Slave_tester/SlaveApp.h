#ifndef SLAVE_APP_H
#define SLAVE_APP_H

#include <Arduino.h>
#include <Wire.h>

#include "configI2C.h"

//#define DEBUG_DUE_SLAVE

class SlaveApp{
  protected:
    volatile int incoming = 0;
    volatile bool dataRequest = false;
    void blinkN(int n);
    String received;
  public:
    static void receiveEvent(int);
    static void requestEvent();
    static SlaveApp *thisPtr;
    
    SlaveApp();
    void mainLoop();
};

#endif
