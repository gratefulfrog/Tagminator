#include "SlaveApp.h"

SlaveApp* SlaveApp::thisPtr;

void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

void SlaveApp::receiveEvent(int howMany){
  thisPtr->incoming  = howMany;    // receive byte as an signed char
}
void SlaveApp::requestEvent(){
  thisPtr->dataRequest = true;
  const char* cStr = thisPtr->received.c_str();
  Wire.write(cStr);
   
}

SlaveApp::SlaveApp(){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,0);
  SlaveApp::thisPtr = this;
  Wire.begin(SLAVE_ADDRESS);                // join i2c bus with address #8
  Wire.onReceive(SlaveApp::receiveEvent);   // register event
  Wire.onRequest(SlaveApp::requestEvent);
  Serial.begin(SERIAL_BAUD_RATE);
  delay(10);
  Serial.println("Slave: running...");
}

void SlaveApp::mainLoop(){
  int localCount = 0;
  if (incoming != localCount){
    localCount = incoming;
    incoming = 0;
    Serial.println("Slave: Receiving: " + String(localCount) + " bytes from master.");
    while(Wire.available()) {
        received += char(Wire.read());
    }
    Serial.println("Bytes received as chars: " + received);
    //blinkN(localCount); // just to see visualize how many bytes were received
  }
  if (dataRequest){
    Serial.println("Slave: data request received!");
    Serial.println("Slave: wrote: " + String(received.length()) + " bytes to master: " + received);
    dataRequest = false;
    received = ""; 
  }
}

void SlaveApp::blinkN(int n){
  n= 2*n;
  while(n--){
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delayMS(500);
  }
}
  
