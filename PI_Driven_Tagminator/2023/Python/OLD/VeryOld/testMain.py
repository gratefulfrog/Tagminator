#!/usr/bin/python3

### THREADED Version way too slow !!!!
## abandoned !!

from time import sleep
import serial
import queue
import threading
import sys
import gui
from collections import OrderedDict
from TaggyLogger import Logger

TAG_READER_BAUD     = 115200
LABEL_DETECTOR_BAUD = 115200

class SerialServer(threading.Thread,Logger):
    """
    Implent the serving of the serial ports for Taggy's DUE and its Tag Reader
    """
    def __init__(self, portT, dataOutQ, mutex, bd=115200, to =1):
        threading.Thread.__init__(self,daemon=True)
        Logger.__init__(self,mutex,'Due Serial Server') # if dataInQ else 'Tag Reader Serial Server')
        self.port        = portT
        self.baudrate    = bd
        self.timeout     = to
        #self.shedSNEvent = shedSNEvent
        self.outQ        = dataOutQ
        """if dataInQ:
            self.run     = self.runDue
            self.inQ     = dataInQ
        else:
            self.run     = self.runTagReader
        """
    
    def run(self):
        """ version for testing
        """
        with serial.Serial(port = self.port,
                           baudrate= self.baudrate,
                           timeout = self.timeout) as ser:
            sleep(0.5)
            while True:
                try:
                    print(ser.in_waiting)
                    incomingSerialString = ser.readline()
                    #if self.shedSNEvent.is_set(): # shed reads from serial
                    #    debugMsg = '** SHED\n* shedding tag sn: <start>' +incomingSerialString + '<end>'
                    #    self.logMessage(debugMsg)
                    #    continue
                    if incomingSerialString:
                        #incomingSerialString = 'SN_' + incomingSerialString
                        self.outQ.put(incomingSerialString[:-1].decode('utf-8'))
                        #debugMsg = '** TAG\n* putting this on outQ: <start>'+ incomingSerialString + '<end>'
                        #self.logMessage(debugMsg)
                except Exception as e:
                    print(e)
                    print('\n* SerialServer thread exiting on exception...')

    def runDue(self):
        """
        clears any incoming stuff, gives the handshake if required, and serves until shut down by someone
        """
        with serial.Serial(port = self.port,
                           baudrate= self.baudrate,
                           timeout = self.timeout) as ser:
            sleep(1)
            while True:
                try:
                    incomingSerialString = ser.readline().decode('utf-8')
                    if incomingSerialString:
                        self.outQ.put(incomingSerialString)
                        debugMsg = '** LABEL\n* got this from serial: <start>' +incomingSerialString +'<end>'
                        self.logMessage(debugMsg)
                        if incomingSerialString[0] == '0':
                            self.shedSNEvent.set()
                            debugMsg = '** END of RUN\n* End of run. Setting Shed Event to enable tag sn shedding.'
                            self.logMessage(debugMsg)
                        debugMsg = 'putting this on outQ: <start>'+ incomingSerialString + '<end>'
                        self.logMessage(debugMsg)
                        
                    try:
                        incoming = self.inQ.get(False)
                    except queue.Empty:
                        pass
                    else:
                        self.inQ.task_done()
                        ser.write(incoming.encode())
                        if '1' in incoming or '2' in incoming:
                            self.shedSNEvent.clear()
                            debugMsg = '** START of RUN\n* Start of run. Clearing Shed Event to disable tag sn shedding.'
                            self.logMessage(debugMsg)
                        elif '3' in incoming:
                            self.shedSNEvent.set()
                            debugMsg = '** STOPPED\n** Run is stopped. Setting Shed Event to enable tag sn shedding.'
                            self.logMessage(debugMsg)
                        elif '4' in incoming:
                            self.shedSNEvent.set()
                            debugMsg = '** PAUSED\n* Run is paused. Setting Shed Event to enable tag sn shedding.'
                            self.logMessage(debugMsg)
                        debugMsg = 'got this from gui: <start>'+ incoming + '<end>' 
                        self.logMessage(debugMsg)
                except Exception as e:
                    print(e)
                    print('\n* SerialServer thread exiting on exception...')


class DataProcessorThread(threading.Thread,Logger):
    """
    Implent the processing of data and putting on GuiQ
    """
    def __init__(self,dataInQ, mutex):
        threading.Thread.__init__(self,daemon=True)
        Logger.__init__(self,mutex,'Data Processor Thread')
        self.inQ         = dataInQ
        #self.outQ        = dataOutQ
        self.debug       = True
        self.lastItem    = None

    def sendToGui(self,item):
        self.outQ.put(item)
        
        
    def processIncoming(self,item,snODict):
        """ the expected sequencing is
        SN1
        LABEL1
        SN2
        LABEL2
        SN4
        LABEL4
        The bad label sequence is
        SN1
        LABEL1
        LABEL2
        SN3
        LABEL3
        the peeking look ahead problem sequnce is:
        SN1
        SN2
        LABEL1
        SN2 or SN3 :  or not? does it continue to look ahead or simply send the same sn twice
        LABEL2
        SN4
        LABEL3 
        so we need to store the SNs in a fifo, unique instance queue,
        Then the algo is:
        item is SN : into Q if not already present
        item is L: get top SN from q and assoc, if qu empty, BAD label
        """
        #msg = 'processing: <start>' + str(item) + '<end> from inQ'
        #self.logMessage(msg)
        if 'Label' in item:
            print(self.lastItem)
            self.lastItem = None
        else:
            self.lastItem = item
        """
        msgBase = 'sent to Gui: <start>'
        if 'SN' in item: # it's a SN !!
            key = item.strip().replace('SN_','')
            snODict[key]=None
        else: # its a label number!
            labelNumber = item[:-1].strip()
            sn = None
            try:
                sn = snODict.popitem(False)[0] # get the first from the start of the dit
            except:
                pass
            if sn != None:
                self.sendToGui([labelNumber, sn])
                msg += str([labelNumber, sn]) 
            else:
                self.sendToGui([labelNumber, 'BAD_LABEL'])
                msg += str([labelNumber, 'BAD_LABEL'])
            msg += '<end>'
        self.logMessage(msg)
        """
        
    def run(self):
        #print('processor running..')
        snOrderedDict = None
        while True:
            try:
                item = self.inQ.get()
            except queue.Empty:
                pass
            else:
                self.inQ.task_done()
                #if snOrderedDict == None or item[:-1].strip() == '0':
                #    snOrderedDict = OrderedDict() 
                self.processIncoming(item,snOrderedDict)


def doTestCommands(dueInQ,labelCount=1000,testPause=10, test = False):
    count = 2 # do it twice
    limitMsg = '0'+str(labelCount)
    if not test:
        print('This is not a test.')
        sleep(1)
        print('I repeat, this is NOT a test.')
        sleep(1)
        print('dueInQ.put(\'' + limitMsg +'\') # LIMIT IS ' + str(labelCount))
        dueInQ.put(limitMsg)
        sleep(1)
        print('dueInQ.put(\'1\')    # FORWARD until label limit')
        dueInQ.put('1')
        sleep(labelCount)
        print('Done')
    else:
        print('dueInQ.put(\'' + limitMsg +'\') # LIMIT IS ' + str(labelCount))
        dueInQ.put(limitMsg)
        sleep(1)
        

        while True and count:
            print('Starting a test cycle! ' + str(count) + ' cycles remaining')
            print('dueInQ.put(\'1\')    # FORWARD for ' + str(testPause) + ' seconds')
            dueInQ.put('1')
            sleep(testPause)
            print('dueInQ.put(\'4\')    # PAUSE for ' + str(testPause) + ' seconds')
            dueInQ.put('4')  #pause
            sleep(testPause)
            print('dueInQ.put(\'1\')    # FORARD for ' + str(testPause) + ' seconds')
            dueInQ.put('1')
            sleep(testPause)
            print('dueInQ.put(\'3\')    # STOP for ' + str(testPause) + ' seconds')
            dueInQ.put('3')
            sleep(testPause)
            count -=1
            print('Test cycle complete')

def showHelp():
        print('Usage: $ ./serialServer.py <DUE_port TagReader_port nbLabels testPauseTimeInSeconds>  default to /dev/ttyACM0 /dev/ttyACM1 100 10'  )
        print('                           if a pauseTime is given then test mode is activated')
        print('examples;')
        print('Usage: $ ./serialServer.py                                  # uses default ports, test pause time, and number of labels')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9        # use these DUE on ttyACM7 end tag reader on ttyACM9')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9   100  # use these ports and a 100 labels not a test')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9   1000 100  # use these ports, 1000 labels and 20 second pause, this implies that it is a test!')
        print('Usage: $ ./serialServer.py -h                               # print this help message')


def startup(_detectorPort,_tagReaderPort):
    #shedSNEvent = threading.Event()
    #shedSNEvent.set()
    #guiInQ = queue.Queue()
    mutex = threading.Lock()
    outQ = queue.Queue()
    #inQ = queue.Queue()
    detectorPort  = _detectorPort  # '/dev/ttyACM0'
    tagReaderPort = _tagReaderPort #'/dev/ttyACM1'
    
    tagReaderServer = SerialServer(tagReaderPort,outQ,mutex,bd=TAG_READER_BAUD,to=None)
    #detectorServer  = SerialServer(detectorPort,outQ,mutex,bd=LABEL_DETECTOR_BAUD,to=None)
    processor       = DataProcessorThread(outQ,mutex)
    tagReaderServer.start()
    #detectorServer.start()
    processor.start()
    while True:
        pass
    #guiG = gui.TaggyGui(mutex,guiInQ,dueInQ)
    #guiG.mainloop()


if __name__ == '__main__':
    dp = None
    trp = None
    if any(['-h' in sys.argv, '--h' in sys.argv, '--help' in sys.argv]):
        showHelp()
        sys.exit(0)

    if len(sys.argv) == 3:
        dp = sys.argv[1]
        trp = sys.argv[2]
    else:    
        import tools.findACM as facm
        dp = facm.getDuePort()
        trp = facm.getTagReaderPort()
        if not dp:
            print('Due not found...')
        elif not trp:
            print('Tag Reader not found...')
    
    if all((dp,trp)):
        print('Label Detector port : ', dp)
        print('Tag Reader Port     : ', trp)
        print ('Checking Serial Ports for availability..')
        waitingD = True
        waitingT = True
        while waitingD or waitingT:
            try:
                sLd = serial.Serial(dp)
                sLd.close()
                waitingD = False
                sTr =  serial.Serial(trp)
                sTr.close()
                waitingT = False
            except Exception as e:
                if waitingD:
                    print('Waiting on Label Detector port...')
                if waitingT:
                    print('Waiting on Tag Reader port...')
            sleep(1)

        print('Starting up...')
        startup(dp,trp)
    else:
        print('Ports not found!')
        sys.exit(0)
           
