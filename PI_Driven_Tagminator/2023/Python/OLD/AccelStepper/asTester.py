#!/usr/bin/python3

import AccelStepper
import RPi.GPIO as GPIO
import time
import sys

# GPIO pins
stepPin = 24 
dirPin  = 25

#speeds

maxSpeed = 900
nbRevs   = 200

def getAS():
    a = AccelStepper.AccelStepper(AccelStepper.AccelStepper.DRIVER,stepPin,dirPin,enable=5)
    return a

def accelerate(asteper, targetSpeed,cw):
    print('setting speed: ',targetSpeed, '  current speed: ',a.getSpeed(), ' cw: ',cw)
    if cw:
        if targetSpeed < a.getSpeed():
            a.setSpeed(targetSpeed)
        else:
            a.setSpeed(a.getSpeed()+50)
    else:
        if targetSpeed > a.getSpeed():
            a.setSpeed(targetSpeed)
        else:
            a.setSpeed(a.getSpeed()-50)
    return

if __name__ == '__main__' :
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    a = getAS()

    s2set = maxSpeed
    sPCT  = 100
    r2s   = nbRevs
    if len(sys.argv) > 1:
        sPCT = max(0,min(100,int(sys.argv[1])))
        s2set = int(s2set*sPCT/100)
    if len(sys.argv) > 2:
       r2s = int(sys.argv[2])

    print('Speed: ', sPCT, '%  Steps: ', r2s)
    
    a.setMaxSpeed(maxSpeed)
    a.enableOutputs()

    try:
        while True:
            #a.setSpeed(0.01*maxSpeed)
            s2set = -s2set
            a.setSpeed(s2set)
            count = r2s
            
            while count:
                #print(100*a.getSpeed()/maxSpeed)
                #accelerate(a,s2set,a.getDirection() == AccelStepper.AccelStepper.DIRECTION_CW)
                a.step(1)
                #time.sleep(0.5)
                #a.runSpeed()
                count -=1
    finally:
        a.disableOutputs()
        #GPIO.cleanup() 
