## Attempt at porting AccelStepper C++ library to Python for RPI

from math import sqrt
import time
import RPi.GPIO as GPIO

def micros():
    return 1000*time.perf_counter_ns()
def nanos():
    return time.perf_counter_ns()

def delayMicroseconds(pulseWidth):
    pw = 1000*pulseWidth
    now = nanos()
    while (nanos()-now < pw):
        pass

def constrain(val, min_val, max_val):
    return min(max_val, max(min_val, val))

class AccelStepper(object):

    DIRECTION_CCW = 0
    DIRECTION_CW  = 1

    FUNCTION  = 0
    DRIVER    = 1
    FULL2WIRE = 2
    FULL3WIRE = 3
    FULL4WIRE = 4
    HALF3WIRE = 5
    HALF4WIRE = 6
    
    def moveTo(self, absolute):
        if self._targetPos != absolute:
            self._targetPos = absolute
            self.computeNewSpeed()

    def move(self, relative):
        self.moveTo(self._currentPos + relative)

    # Implements steps according to the current step interval
    # You must call this at least once per step
    # returns true if a step occurred
    def runSpeed(self):
        if (not self._stepInterval):
            return False
        #print("running speed")
        if self._direction == AccelStepper.DIRECTION_CW:
            # Clockwise
            self._currentPos += 1
        else:
            # Anticlockwise  
            self._currentPos -= 1
        self.step(self._currentPos)
        return True
        
    def distanceToGo(self):
        return self._targetPos - self._currentPos

    def targetPosition(self):
        return self._targetPos

    def currentPosition(self):
        return self._currentPos

    # Useful during initialisations or after initial positioning
    # Sets speed to 0
    def setCurrentPosition(self, position):
        self._targetPos = self._currentPos = position
        self._n = 0
        self._stepInterval = 0
        self._speed = 0.0

    # Subclasses can override
    def computeNewSpeed(self):
        #print('computing new speed')
        #print(self._stepInterval)
        distanceTo = self.distanceToGo() # +ve is clockwise from curent location
        stepsToStop = ((self._speed * self._speed) / (2.0 * self._acceleration)) # Equation 16
        if distanceTo == 0 and stepsToStop <= 1:
            # We are at the target and its time to stop
            self._stepInterval = 0
            self._speed = 0.0
            self._n = 0
            return # self._stepInterval

        if (distanceTo > 0):
	    # We are anticlockwise from the target
	    # Need to go clockwise from here, maybe decelerate now
            if (self._n > 0):
                # Currently accelerating, need to decel now? Or maybe going the wrong way?
                if ((stepsToStop >= distanceTo) or (self._direction == AccelStepper.DIRECTION_CCW)):
                    self._n = -stepsToStop
                    # Start deceleration
            elif (self._n < 0):
                # Currently decelerating, need to accel again?
                if ((stepsToStop < distanceTo) and self._direction == AccelStepper.DIRECTION_CW):
                    self._n = -self._n # Start accceleration
        elif (distanceTo < 0):
	    # We are clockwise from the target
	    # Need to go anticlockwise from here, maybe decelerate
            if (self._n > 0):
	    # Currently accelerating, need to decel now? Or maybe going the wrong way?
                if ((stepsToStop >= -distanceTo) or self._direction == AccelStepper.DIRECTION_CW):
                    self._n = -stepsToStop # Start deceleration
            elif (self._n < 0):
		# Currently decelerating, need to accel again?
                if ((stepsToStop < -distanceTo) and self._direction == AccelStepper.DIRECTION_CCW):
                    self._n = -self._n # Start accceleration
        # Need to accelerate or decelerate
        if (self._n == 0):
    	    # First step from stopped
            self._cn = self._c0
            self._direction =  AccelStepper.DIRECTION_CW if (distanceTo > 0) else AccelStepper.DIRECTION_CCW
        else:
    	    # Subsequent step. Works for accel (n is +_ve) and decel (n is -ve).
            self._cn = self._cn - ((2.0 * self._cn) / ((4.0 * self._n) + 1)) # Equation 13
            self._cn = max(self._cn, self._cmin)
    
        self._n +=1
        self._stepInterval = self._cn
        self._speed = 1000000.0 / self._cn
        if (self._direction == AccelStepper.DIRECTION_CCW):
            self._speed = -self._speed
        print(self._stepInterval)
        return #self._stepInterval

    def run(self):
        if self.runSpeed():
            self.computeNewSpeed()
        return self._speed != 0.0 or self.distanceToGo() != 0

    def __init__(self, interface, pin1, pin2, pin3=None, pin4=None, enable=None,forward=True,backward=None):
        self._interface     = interface
        self._currentPos    = 0
        self._targetPos     = 0
        self._speed         = 0.0
        self._maxSpeed      = 0.0
        self._acceleration  = 0.0
        self._sqrt_twoa     = 1.0
        self._stepInterval  = 0
        self._minPulseWidth = 10
        self._enablePin     = enable
        #self._lastStepTime  = 0
        self._pin           = [pin1,pin2,pin3,pin4]
        self._forward       = forward
        self._backward      = backward
        if self._forward and self._backward:
            self._pin       = [0,0,0,0]
        self._enableInverted = True
    
        # NEW
        self._n = 0
        self._c0 = 0.0
        self._cn = 0.0
        self._cmin = 1.0
        self._direction = AccelStepper.DIRECTION_CCW
        self._pinInverted = [0 for i in range(4)]
        
        if (enable):
            self.enableOutputs()
        # Some reasonable default
        self.setAcceleration(1)
        self.setMaxSpeed(1)
        
    def setDirection(self, dire):
        self._direction = dire
    
    def getDirection(self):
        return self._direction

    def setMaxSpeed(self,speed):
        if (speed < 0.0):
            speed = -speed
        if self._maxSpeed != speed:
            self._maxSpeed = speed
            self._cmin     = 1000000.0/speed
            # Recompute _n from current speed and adjust speed if accelerating or cruising
            if self._n > 0:
                self._n = ((self._speed * self._speed) / (2.0 * self._acceleration)) # Equation 16
                self.computeNewSpeed()

    def getMaxSpeed(self):
        return self._maxSpeed

    def setAcceleration(self, acceleration):
        if (acceleration == 0.0):
            return
        if (acceleration < 0.0):
            acceleration = -acceleration
        if (self._acceleration != acceleration):
	    # Recompute _n per Equation 17
            self._n = self._n * (self._acceleration / acceleration)
            # New c0 per Equation 7, with correction per Equation 15
            self._c0 = 0.676 * sqrt(2.0 / acceleration) * 1000000.0 # Equation 15
            self._acceleration = acceleration
            self.computeNewSpeed()
    
    def getAcceleration(self):
        return self._acceleration
            
    def setSpeed(self, speed):
        if (speed == self._speed):
            return
        speed = constrain(speed, -self._maxSpeed, self._maxSpeed)
        if (speed == 0.0):
            self._stepInterval = 0
        else:
            self._stepInterval = abs(1000000.0 / speed)
            self._direction = AccelStepper.DIRECTION_CW if (speed > 0.0) else AccelStepper.DIRECTION_CCW
        self._speed = speed
        #print('speed', speed)
        #print('self._stepInterval', self._stepInterval)
        #time.sleep(2)
        
    def getSpeed(self):
        return self._speed

    def step(self,step):
        stepFuncV = [self.step0,
                     self.step1,
                     self.step2,
                     self.step3,
                     self.step4,
                     self.step6,
                     self.step8]
        delayMicroseconds(self._stepInterval)
        stepFuncV[self._interface](step)

    def stepForward(self):
        # Clockwise
        self._currentPos +=1
        self.step(self._currentPos)
        #self._lastStepTime = micros()
        return self._currentPos

    def stepBackward(self):
        # Counter-clockwise
        self._currentPos -= 1
        self.step(_currentPos)
        #self._lastStepTime = micros()
        return self._currentPos

    def setOutputPins(self,mask):
        numpins = 2
        if (self._interface == AccelStepper.FULL4WIRE or self._interface == AccelStepper.HALF4WIRE):
            numpins = 4
        elif (self._interface == AccelStepper.FULL3WIRE or  self._interface == AccelStepper.HALF3WIRE):
            numpins = 3
        for i in range (numpins):
            GPIO.output(self._pin[i],
                        (GPIO.HIGH ^ self._pinInverted[i]) if (mask & (1 << i)) else
                        (GPIO.LOW ^ self._pinInverted[i]))

    # 0 pin step function (ie for functional usage)
    def step0(self, step):
        if (self._speed > 0):
            self._forward()
        else:
            self._backward()

    # 1 pin step function (ie for stepper drivers)
    # This is passed the current step number (0 to 7)
    # Subclasses can override
    def step1(self, step):
        # _pin[0] is step, _pin[1] is direction
        # Set direction first else get rogue pulses
        self.setOutputPins(0b10 if self._direction else 0b00) 
        self.setOutputPins(0b11 if self._direction else 0b01) # step HIGH
        # Caution 200ns setup time 
        # Delay the minimum allowed pulse width
        delayMicroseconds(self._minPulseWidth)
        self.setOutputPins(0b10 if self._direction else 0b00) # step LOW

    # 2 pin step function
    # This is passed the current step number (0 to 7)
    # Subclasses can override
    def step2(self, step):
        pinSettingsVec = [0b10,0b11,0b01,0b00]
        self.setOutputPins(pinSettingsVec[step & 0x3])

    # 3 pin step function
    # This is passed the current step number (0 to 7)
    # Subclasses can override
    def step3(self, step):
        pinSettingsVec = [0b100,0b001,0b010]
        self.setOutputPins(pinSettingsVec[step % 3])

    # 4 pin step function for half stepper
    # This is passed the current step number (0 to 7)
    #Subclasses can override
    def step4(self, step):
        pinSettingsVec = [0b0101,0b0110,0b1010,0b1001]
        self.setOutputPins(pinSettingsVec[step & 0x3])

    # 6 pin half step function
    # This is passed the current step number (0 to 7)
    # Subclasses can override
    def step6(self,step):
        pinSettingsVec = [0b100,0b101,0b001,0b011,0b010,0b110]
        self.setOutputPins(pinSettingsVec[step % 6])
        
    # 8 pin half step function
    # This is passed the current step number (0 to 7)
    # Subclasses can override
    def step8(self, step):
        pinSettingsVec = [0b0001,0b0101,0b0100,0b0110,0b0010,0b1010,0b1000,0b1001]
        self.setOutputPins(pinSettingsVec[step & 0X7])

    # Prevents power consumption on the outputs
    def disableOutputs(self):
        if (not self. _interface):
            return
        self.setOutputPins(0) # Handles inversion automatically
        if (self._enablePin != 0xff):
            GPIO.setup(self._enablePin, GPIO.OUT)
            GPIO.output(self._enablePin, GPIO.LOW ^ self._enableInverted)

    def enableOutputs(self):
        if (not self. _interface) :
            return
        GPIO.setup(self._pin[0], GPIO.OUT)
        GPIO.setup(self._pin[1], GPIO.OUT)
        if (self._interface == AccelStepper.FULL4WIRE or  self._interface == AccelStepper.HALF4WIRE):
            GPIO.setup(self._pin[2], GPIO.OUT)
            GPIO.setup(self._pin[3], GPIO.OUT)
        elif (self._interface == AccelStepper.FULL3WIRE or  self._interface == AccelStepper.HALF3WIRE):
            GPIO.setup(self._pin[2], GPIO.OUT)
        if (self._enablePin != 0xff):
            GPIO.setup(self._enablePin, GPIO.OUT) 
            GPIO.output(self._enablePin, GPIO.HIGH ^ self._enableInverted)
    
    def setMinPulseWidth(self, minWidth):
        self._minPulseWidth = minWidth

    def setEnablePin(self, enablePin):
        self._enablePin = enablePin
        # This happens after construction, so init pin now.
        if (self._enablePin != 0xff):
            GPIO.setup(self._enablePin, GPIO.OUT) 
            GPIO.output(self._enablePin, GPIO.HIGH ^ self._enableInverted)

    def setPinsInverted(self,
                        pin1Invert = None,
                        pin2Invert = None,
                        pin3Invert = None,
                        pin4Invert = None,
                        enableInvert = None):
        self._pinInverted[0] = pin1Invert
        self._pinInverted[1] = pin2Invert
        self._pinInverted[2] = pin3Invert
        self._pinInverted[3] = pin4Invert
        self._enableInverted = enableInvert

    # Blocks until the target position is reached and stopped
    def runToPosition(self):
        while self.run():
            pass

    def runSpeedToPosition(self):
        if (self._targetPos == self._currentPos):
            return False
        if (self._targetPos >self._currentPos):
            self._direction = AccelStepper.DIRECTION_CW
        else:
            self._direction = AccelStepper.DIRECTION_CCW
        return self.runSpeed()

    # Blocks until the new target position is reached
    def runToNewPosition(self, position):
        self.moveTo(position)
        self.runToPosition()

    def stop(self):
        if (self_speed != 0.0):
            stepsToStop = ((self._speed * self._speed) / (2.0 * self._acceleration)) + 1 #  Equation 16 (+integer rounding)
            if (self._speed > 0):
                self.move(stepsToStop)
            else:
                self.move(-stepsToStop)

    def isRunning(self):
        return not (self._speed == 0.0 and self._targetPos == self._currentPos)
