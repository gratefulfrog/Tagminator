#!/usr/bin/python3

import time 
import sys
import signal
from config import Config

minT = Config.speedMeasurementTime  # seconds

debug = True

class SpeedControl():
    def __init__(self,initialLabelCount=0):
        self.deltaMin    = minT # in seconds
        self.reset(initialLabelCount)
        
    def reset(self,initialLabelCount = 0):
        self.lastCount     = initialLabelCount
        self.speedFactor   = 1
        self.lastSpeedTime = 0
        self.initialSpeed  = 0
        self.deltaCount    = 0
        self.currentSpeed  = 0
        
    def updateSpeedFactor(self,currentCount):
        if not self.lastSpeedTime:
            self.lastSpeedTime = time.time()

        now = time.time()
        deltaT = now - self.lastSpeedTime
        if (deltaT >= minT):
            self.deltaCount = currentCount - self.lastCount
            if not self.initialSpeed:
                self.initialSpeed = self.deltaCount/deltaT
            self.lastCount  = currentCount
            self.lastSpeedTime = now
            self.currentSpeed = self.deltaCount/deltaT   #labels per second
            self.speedFactor = self.currentSpeed/self.initialSpeed
            if debug:
                self.showSpeedFactor()
            return True
        return False

    def showSpeedFactor(self):
        print(f'delta Label Count    : {self.deltaCount}')
        print(f'Current Labels/sec   : {round(self.currentSpeed,2)}')
        print(f'New Speed Factor     : {round(self.speedFactor,2)}')
        print(f'Corrected Labels/sec : {round(self.currentSpeed/self.speedFactor,2)}')
        

def signal_handler(sig, frame):
    print('\nclean exit..')
    sys.exit(0)
                        
def startup():
    signal.signal(signal.SIGINT, signal_handler)
    inst = SpeedControl()
    count = 1
    while True:
        inst.updateSpeedFactor(count)
        time.sleep(1)
        count *=2

if __name__ == '__main__':
    startup()


