#!/usr/bin/python3

from time import time,sleep
import serial
from multiprocessing import Process, JoinableQueue
import sys
import pigpio
import signal
import queue

import pigStepper
import gui
from config import Config


TAG_READER_BAUD     = Config.TagReaderBaud
DETECTOR_PIN        = Config.LabelDetectedPin


def pauseS(sec):
    now = time();
    while now - time() < sec:
        pass

class SerialServer(Process):
    """
    Implent the serving of the serial ports for Taggy's DUE and its Tag Reader
    """
    def __init__(self, portT, dataOutQ, bd=115200, to =1):
        Process.__init__(self,daemon=True)
        #Logger.__init__(self,mutex,'Due Serial Server') # if dataInQ else 'Tag Reader Serial Server')
        self.port        = portT
        self.baudrate    = bd
        self.timeout     = to
        self.outQ        = dataOutQ
    
    def run(self):
        """ version for testing
        """
        with serial.Serial(port = self.port,
                           baudrate= self.baudrate,
                           timeout = self.timeout) as ser:
            sleep(1)
            while True:
                try:
                    incomingSerialString = ser.readline()
                    if incomingSerialString:
                        #print(incomingSerialString.strip().decode('utf-8'))
                        self.outQ.put(incomingSerialString.strip().decode('utf-8'))
                except Exception as e:
                    print(e)
                    print('\n* SerialServer thread exiting on exception...')

class LabelDetectorSerialServer(Process):
    """
    Implent the serving of the interrupts from the label detector
    """
    def __init__(self, piggy, dataOutQ, inputPin):
        Process.__init__(self,daemon=True)
        self.pig = piggy
        self.outQ =  dataOutQ
        self.count = 1
        self.header = 'Label: '
        self.msg = ''
        self.inputPin = inputPin
        self.pig.set_mode(inputPin, pigpio.INPUT)
        self.detectArmed = False #self.pig.read(inputPin)
        #self.pigCallback = self.pig.callback(inputPin, pigpio.RISING_EDGE,self.labelDetectHandler)
        self.pigCallback = self.pig.callback(inputPin, pigpio.EITHER_EDGE,self.labelDetectHandler)
        signal.signal(signal.SIGINT, self.signal_handler)
        
    """
    def labelDetectHandler(self,gpio, level, tick):            
        self.msg = self.header + str(self.count)
        self.outQ.put(self.msg)
        #print('LableDetectorSerialServer: ' + self.msg)
        self.count+=1
    """
    def labelDetectHandler(self,gpio, level, tick):
        if not level and self.detectArmed: 
            self.msg = self.header + str(self.count)
            self.outQ.put(self.msg)
            #print('LabelDetectorSerialServer: ' + self.msg)
            self.count+=1
            self.detectArmed = False
        else:
            self.detectArmed = True
    
    def signal_handler(self, sig, frame):
        self.pig.stop()
        print('\nclean exit..')
        sys.exit(0)
            
    def run(self):
        """ version for input pin interrurp testing
        """
        signal.pause()

class DataProcessorThread(Process):
    """
    Implent the processing of data and putting on GuiQ
    """
    def __init__(self,dataInQ, dataOutQ):
        Process.__init__(self,daemon=True)
        #Logger.__init__(self,mutex,'Data Processor Thread')
        self.inQ         = dataInQ
        self.outQ        = dataOutQ
        self.debug       = True
        self.lastItem    = None
        self.lastLabel   = None
        self.lastLastSN  = None
        self.labelCount = 0
        #self.init = False

    def sendToGui(self,item):
        self.outQ.put(item)
        
    def destructureLastItem_a(self):
        try:
            if self.lastItem:
                incoming = self.lastItem.strip() # now we have E0123456789ABCDEF 00_00_00_80 NN
                #print('icoming : ',incoming)
                incomingV = incoming.split(' ')
                sn = tampV = nbReads = None
                if len(incomingV) == 3:
                    sn,tampV,nbReads = incomingV
                    #tampV  = 'False' if tampV in ['00000088','00000080'] else 'True '
                    tampV  = 'True ' if tampV[4] == '8' else 'False' # in ['00000088','00000080'] else 'True '
                elif len(incomingV) == 2:
                    sn,tampV,nbReads = incomingV+[None]
                    #tampV  = 'False' if tampV in ['00000088','00000080'] else 'True '
                    tampV  = 'True ' if tampV[4] == '8' else 'False' # in ['00000088','00000080'] else 'True '
                elif len(incomingV) == 1:
                    sn,tampV,nbReads = incomingV+[None,None]
                return sn,tampV,nbReads
        except Exception as e:
            print(e)
            print('icoming : ',incoming)
        return None,None,None

    def destructureLastItem(self):
        if self.lastItem:
            sn   = self.lastItem[self.lastItem.find('E'):self.lastItem.find(',')]
            tampV = self.lastItem[self.lastItem.find(',')+1:]
            tamp  = 'False' if tampV in ['00000088','00000080'] else 'True '
            nbReads = self.lastItem[0:self.lastItem.find(':')]
            return sn,tamp,nbReads
        return None

    def structure4Gui(self,ID,sn=None,tamp=None,nbr=None):
        # output is a list: [labelNBR,sn,tamperedStatus,nbrReads]
        #                   [0,None,None,None]
        #                   ['1','E0123456789ABCDEF','False','54']
        return ([ID,sn,tamp,nbr])

    def processIncoming(self,item):
        self.lastLabel = item + ' '
        if 'Label' in item:  # so we can print the previous
            outMsg = self.lastLabel
            lid = self.lastLabel[self.lastLabel.find(' '):].strip()
            sn = tamp = nbr= None
            if self.lastItem is not None:
                # test to see if lastSN is new
                sn,tamp,nbr = self.destructureLastItem_a()
                if self.lastLastSN !=sn:
                    outMsg = outMsg + 'SN: ' + sn if sn is not None else '' + \
                    ' Tampered: ' + tamp if tamp is not None else ''  + \
                    ' Nb Reads: ' + nbr if nbr is not None else ''
                    self.lastLastSN = sn
            else:
                outMsg = outMsg + 'None'
            self.outQ.put(self.structure4Gui(lid,sn,tamp,nbr))
            self.lastItem = None
            self.labelCount += 1
        else:
            self.lastItem = item

    def run(self):
        print('processor running..')
        while True:
            try:
                item = self.inQ.get()
            except queue.Empty:
                pass
            else:
                self.inQ.task_done()
                self.processIncoming(item) # trailing line feed or cr


class PigServer(Process):
    """
    test of pigstepping
    """
    def __init__(self, pig, dataQ):
        Process.__init__(self,daemon=True)
        self.Q          = dataQ
        self.pigStepper = pigStepper.PigStepper(pi=pig)
        print(self.pigStepper)
    
    def run(self):
        """ version for production
        """
        print('PigServer Started!')
        try:
            while True:
                #self.pigStepper.step()
                #print('step')
                try:
                    item = self.Q.get() #False)
                except queue.Empty:
                    pass
                else:
                    self.Q.task_done()
                    self.processIncoming(int(item)) # trailing line feed or cr
        except KeyboardInterrupt:
            self.pigStepper.enable(False)
            print('stepper disabled!')
            sys.exit(0)
            
    def processIncoming(self,nb):
        self.pigStepper.enable(nb!=0)
        #self.pigStepper.setDirection(nb>=0)
        self.pigStepper.setDirection(nb<0)
        self.pigStepper.setSpeed(abs(nb))
        #print(self.pigStepper)
                
def showHelp():
    print('Usage: $ ./procTestMain.py <TagReader_port>')
    print('for example:')
    print('    $ ./procTestMain.py /dev/ttyACM1')

def startup(_tagReaderPort):
    outQ    = JoinableQueue()
    guiInQ  = JoinableQueue()
    guiOutQ = JoinableQueue()
    tagReaderPort = _tagReaderPort #'/dev/ttyACM1'
    
    tagReaderServer = SerialServer(tagReaderPort,outQ,bd=TAG_READER_BAUD,to=None)
    processor       = DataProcessorThread(outQ,guiInQ)
    tp = Process(target=tagReaderServer.run,args=())
    p = getPig()
    labelDetector = LabelDetectorSerialServer(p, outQ,DETECTOR_PIN) 
    tagReaderServer.start()
    labelDetector.start()
    processor.start()
    pigServer = PigServer(p,guiOutQ)
    pigServer.start()
    guiG = gui.TaggyGui(guiInQ,guiOutQ)
    guiG.mainloop()

def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      sys.exit()
   return pig

if __name__ == '__main__':
    trp = None
    if any(['-h' in sys.argv, '--h' in sys.argv, '--help' in sys.argv]):
        showHelp()
        sys.exit(0)

    if len(sys.argv) == 2:
        trp = sys.argv[1]
    else:
        showHelp()
        sys.exit(0)
        """
        import tools.findACM as facm
        dp = facm.getDuePort()
        trp = facm.getTagReaderPort()
        if not dp:
            print('Due not found...')
        elif not trp:
            print('Tag Reader not found...')
        """
    if all((trp)):
        print('Tag Reader Port     : ', trp)
        print ('Checking Serial Ports for availability..')
        waitingT = True
        while waitingT:
            try:
                sTr =  serial.Serial(trp)
                sTr.close()
                waitingT = False
            except Exception as e:
                if waitingT:
                    print('Waiting on Tag Reader port...')
            sleep(1)

        print('Starting up...')
        startup(trp)
    else:
        print('Ports not found!')
        sys.exit(0)
           
