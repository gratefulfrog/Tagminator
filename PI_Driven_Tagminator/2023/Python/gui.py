#!/usr/bin/python3

"""
Execute the gui directly from the command line to start the test driver
$ ./gui.py
"""

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import csv

import types
import queue
import threading
import time
from os import getcwd

import serial
from multiprocessing import Process, JoinableQueue
import sys
import pigpio
import signal
import queue

#import pigStepper
import procMain

from config import Config
from speedControl import SpeedControl

#from TaggyLogger import Logger

versionString = 'Taggy Gui: ' + getcwd().split('/')[-2]

defaultStartSpeedPct  =  Config.initialStartSpeedPct #75

badLabelStringVec = [None, 'True ', 'True']  #'BAD_LABEL'
DuplicateIndicator = 'Duplicate'
BadIndicator       = 'Bad'
TamperedIndicator  = 'Tampered'

generalFont        = ('Arial', 8)
listBoxFont        = ('mono', 12)

speedColor         = 'SeaGreen2'
speedTroughColor   = 'SeaGreen3'
activeColor        = 'blue'
forwardColor       = 'green'
reverseColor       = 'cyan4'
entryColor         = 'white'
limitColor         = 'SeaGreen1'
limitActiveColor   = 'cornsilk3'
limitErrorColor    = 'red'
stopColor          = 'red'
pauseColor         = 'yellow'
listColor          = 'gray80'
badTagColor        = 'red'
tamperedTagColor   = 'orange2'
saveColor          = 'cornsilk3'
quitColor          = 'cornsilk4'
countColor         = ['sea green',       # 0
                      'PaleGreen4',      # 1
                      'PaleGreen3',      # 2
                      'PaleGreen2',      # 3
                      'PaleGreen1',      # 4
                      'goldenrod1',      # 5
                      'goldenrod2',      # 6
                      'goldenrod3',      # 7
                      'goldenrod4',      # 8
                      'firebrick1',      # 9
                      'firebrick2',      # 10
                      'firebrick3',      # 11
                      'firebrick4',      # 12
                      'orange red']      # 13

def getCountColor(nbGood=0,nbBad=0):
    if nbBad == 0:
        return countColor[0]
    else:
        failurePct = min(len(countColor)-1,round(100*nbBad/(nbGood+nbBad)))
        return countColor[failurePct]

def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      sys.exit()
   return pig

### simulation variables
listBoxPollDelay = 100 # milliseconds
maxSNLength  = 30
maxQWait     = 5 # seconds



#class TaggyGui(tk.Frame,Logger):
class TaggyGui(tk.Frame):
    """
    commandDict = {'l' : '0',    # followed by argument as a string!
                   'f' : '1',    # forward
                   'r' : '2',    # reverse
                   's' : '3',    # stop
                   'p' : '4',    # pause
                   'v' :  None,  # not implemented
                   'h' :  None,  # not implemented
                   'g' : '6'}    # followed by arg as string 
    """
    def checkTagReaderPort(self):
        print('Tag Reader Port     : ', Config.TagReaderPort)
        print ('Checking for availability..')
        try:
            sTr =  serial.Serial(Config.TagReaderPort)
            sTr.close()
        except Exception as e:
            print(f'failed to open Tag Reader port: {Config.TagReaderPort}')
            sys.exit(0)
            
    def createSubProcesses(self):
        self.checkTagReaderPort()
        self.tagReaderServer = procMain.SerialServer(Config.TagReaderPort,self.procOutQ,bd=Config.TagReaderBaud,to=None)
        self.processor       = procMain.DataProcessorThread(self.procOutQ,self.inQ)
        self.tp = Process(target=self.tagReaderServer.run,args=())
        self.p = getPig()
        self.labelDetector = procMain.LabelDetectorSerialServer(self.p, self.procOutQ,Config.LabelDetectedPin) 
        self.tagReaderServer.start()
        self.labelDetector.start()
        self.processor.start()
        self.pigServer = procMain.PigServer(self.p,self.outQ)
        self.pigServer.start()

        
    def __init__(self,procOutQ,inQ,outQ,master=None):
        tk.Frame.__init__(self, master)
        self.master.title("Tagminator")
        self.master.geometry('610x400') 
        self.grid()
        self.procOutQ = procOutQ
        self.inQ      = inQ
        self.outQ     = outQ
        self.createSubProcesses()
        self.currentLimit   = tk.IntVar()
        self.currentSpeed   = tk.IntVar()
        self.countString    = tk.StringVar()
        self.badPause       = tk.IntVar()
        self.tamperedPause  = tk.IntVar()
        self.duplicatePause = tk.IntVar()
        self.widgetDict = {}

        optionFrameArgDict = { 'widget': 'tk.Frame',
                               'argDict' : {'master'  : self.master,
                                            'name'    : 'optionFrame',
                                            'bd'      : 1,
                                            'relief'  : 'sunken'},
                             'gridDict' : {'row'        : 0,
                                           'column'     : 0,
                                           'rowspan'    : 2,
                                           'sticky'     : 'NSEW'}}

        optionLabelArgDict = {  'widget': 'tk.Label',
                                'argDict' : {'master' : lambda : self.widgetDict['optionFrame'],
                                             'name'   : 'optionLabel',
                                             'text'   : 'Pause\nOn',
                                             'bg'     : pauseColor,
                                             'font'   : generalFont,
                                             'height' : 2,
                                             'anchor' : 'w',
                                             'relief' : 'sunken'},
                                'gridDict' : {'row'      : 0,
                                              'column'   : 0,
                                              'rowspan'  : 3,
                                              'sticky'   : 'NSEW'}}

        badButtonnArgDict = {  'widget': 'tk.Checkbutton',
                               'argDict' : {'master'   : lambda : self.widgetDict['optionFrame'],
                                            'name'     : 'badButton',
                                            'text'     : 'Bad',
                                            'variable' : self.badPause,
                                            'bg'       : pauseColor,
                                            'font'     : generalFont,
                                            'anchor'   : 'w',
                                            'relief'   : 'sunken'},
                               'gridDict' : {'row'     : 0,
                                             'column'  : 1,
                                             'sticky'  : 'NSEW'}}
        
        tamperedButtonArgDict = {  'widget': 'tk.Checkbutton',
                                   'argDict' : {'master'   : lambda : self.widgetDict['optionFrame'],
                                                'name'     : 'tamperedButton',
                                                'text'     : 'Tampered',
                                                'variable' : self.tamperedPause,
                                                'bg'       : pauseColor,
                                                'font'     : generalFont,
                                                'anchor'   : 'w',
                                                'relief'   : 'sunken'},
                                   'gridDict' : {'row'     : 1,
                                                 'column'  : 1,
                                                 'sticky'  : 'NSEW'}}
        
        duplicateButtonArgDict = {  'widget': 'tk.Checkbutton',
                                    'argDict' : {'master' : lambda : self.widgetDict['optionFrame'],
                                                 'name'   : 'duplicateButton',
                                                 'text'   : 'Duplicate',
                                                 'bg'     : pauseColor,
                                                 'font'   : generalFont,
                                                 'anchor' : 'w',
                                                 'relief' : 'sunken'},
                                   'gridDict' : {'row'    : 2,
                                                 'column' : 1,
                                                 'sticky' : 'NSEW'}}
        
        limitEntryArgDict = {  'widget'  : 'tk.Entry',
                               'argDict' : {'master'       : self.master,
                                            'name'         : 'limitEntry',
                                            'textvariable' : self.currentLimit,
                                            'bg'           : limitColor,
                                            'font'         : generalFont,
                                            'justify'      : tk.CENTER,
                                            'relief'       : 'sunken'},
                               'gridDict' : {'row'         : 0,
                                             'column'      : 1,
                                             'sticky'      : 'NSEW'},
                               'confFunc' : self.configureLimit}

        limLabelArgDict = {  'widget': 'tk.Label',
                             'argDict' : {'master'  : self.master,
                                          'name'    : 'limitLabel',
                                          'text'    : 'Label Limit',
                                          'bg'      : limitColor,
                                          'font'    : generalFont,
                                          'relief'  : 'sunken'},
                             'gridDict' : {'row'    : 1,
                                           'column' : 1,
                                           'sticky' : 'NSEW'}}
        
        forwardButtonArgDict = {  'widget': 'tk.Button',
                                  'argDict' : {'master' : self.master,
                                               'name'   : 'forwardButton',
                                               'text'   : 'Forward',
                                               'bg'     : forwardColor,
                                               'font'   : generalFont,
                                               'activebackground' : activeColor,                       
                                               'command'          : lambda : self.directionCallBack(0)},
                                  'gridDict' : {'row'    : 0,
                                                'column' : 2,
                                                'sticky' : 'NSEW'}} 

        reverseButtonArgDict = {  'widget': 'tk.Button',
                                  'argDict' : {'master' : self.master,
                                               'name'   : 'reverseButton',
                                               'text'   :  'Reverse',
                                               'bg'     : reverseColor,
                                                'font'  : generalFont,
                                               'activebackground' : activeColor,
                                               'command'          : lambda : self.directionCallBack(1)},
                                  'gridDict' : {'row'    : 1,
                                                'column' : 2,
                                                'sticky' : 'NSEW'}}

        stopButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : self.master,
                                          'name'   : 'stopButton',
                                          'text'   : 'Stopped',
                                          'bg'     : activeColor,
                                          'font'   : generalFont,
                                          'activebackground' : activeColor,
                                          'command'          : self.stopCallBack},
                             'gridDict' : {'row'    : 0,
                                           'column' : 3,
                                           'sticky' : 'NSEW'}}
        
        pauseButtonArgDict = {'name' : 'pauseButton',
                              'widget': 'tk.Button',
                              'argDict' : {'master'  : self.master,
                                           'name'    : 'pauseButton',
                                           'text'    : 'Pause',
                                           'bg'      : pauseColor,
                                           'font'    : generalFont,
                                           'activebackground' : activeColor,
                                           'command'          : self.pauseCallBack},
                              'gridDict' : {'row'    : 1,
                                            'column' : 3,
                                            'sticky' : 'NSEW'}}
        
        ScaleSLiderDict = {  'widget'  : 'tk.Scale',
                             'argDict' : {'master'       : self.master,
                                          'name'         : 'speedSlider',
                                          'bg'           : speedColor,
                                          'troughcolor'  : speedTroughColor,
                                          'label'        : 'Speed (%)',
                                          'sliderrelief' : 'raised',
                                          'relief'       : 'sunken',
                                          'from_'        : 1,
                                          'to'           : 100,
                                          'orient'       : tk.HORIZONTAL,
                                          'length'       : 20, #350,
                                          'showvalue'    : 1,
                                          'resolution'   : 1,
                                          'font'         : generalFont,
                                          'variable'     : self.currentSpeed},
                             'gridDict' : {'row'         : 2,
                                           'column'      : 0,
                                           'columnspan'  : 2,
                                           'sticky'      : 'NSEW'},
                             'confFunc' : self.configureSpeed}

        countLabelArgDict = {  'widget': 'tk.Label',
                               'argDict' : {'master'       : self.master,
                                            'name'         : 'countLabel',
                                            'textvariable' : self.countString,
                                            'bg'           : getCountColor(),
                                            'justify'      : tk.CENTER,
                                            'font'         : generalFont,
                                            'relief'       : 'sunken'},
                               'gridDict' : {'row'         : 2,
                                             'column'      : 2,
                                             'columnspan'  : 2,
                                             'sticky'      : 'NSEW'}}

        listFrameArgDict = { 'widget': 'tk.Frame',
                             'argDict' : {'master'      : self.master,
                                          'name'        : 'listFrame',
                                          'bd'          : 1,
                                          'relief'      : 'sunken'},
                             'gridDict' : {'row'        : 3,
                                           'column'     : 0,
                                           'columnspan' : 4,
                                           'sticky'     : 'NSEW'}}
        
        scrollbarArgDict = { 'widget': 'tk.Scrollbar',
                             'argDict' : {'master' :  lambda : self.widgetDict['listFrame'],
                                          'name'    : 'scrollbar',
                                          'orient'  : tk.VERTICAL},
                             'gridDict' : {'row'        : 0,
                                           'column'     : 1,
                                           'sticky'     : 'NSEW'}}
        
        listboxArgDict = { 'widget': 'tk.Listbox',
                           'argDict' : {'master' :  lambda : self.widgetDict['listFrame'],
                                        'name'    : 'listbox',
                                        'bd'      : 0,
                                        'justify' : tk.LEFT,
                                        'font'    : listBoxFont},
                           'gridDict' : {'row'    : 0,
                                         'column' : 0,
                                         'sticky' : 'NSEW'}}
        
        saveFrameArgDict = { 'widget': 'tk.Frame',
                             'argDict' : {'master'  : self.master,
                                          'name'    : 'saveFrame',
                                          'bd'      : 1,
                                          'relief'  : 'sunken'},
                             'gridDict' : {'row'        : 4,
                                           'column'     : 0,
                                           'columnspan' : 4,
                                           'sticky'     : 'NSEW'}}
        
        saveButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : lambda : self.widgetDict['saveFrame'],
                                          'name'   : 'saveButton',
                                          'text'   : 'Save Data File',
                                          'bg'     :  saveColor,
                                          'font'   : generalFont,
                                          'activebackground' : activeColor,
                                          'command'          : lambda : self.saveFile()},
                             'gridDict' : {'row'    : 0,
                                           'column' : 0,
                                           'sticky' : 'NSEW'}}

        quitButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : lambda : self.widgetDict['saveFrame'],
                                          'name'   : 'quitButton',
                                          'text'   : 'Quit',
                                          'bg'     :  quitColor,
                                          'font'   : generalFont,
                                          'activebackground' : activeColor,
                                          'command'          : self.quit},
                             'gridDict' : {'row'    : 0,
                                           'column' : 1,
                                           'sticky' : 'NSEW'}}
        widgetSpecs = [optionFrameArgDict,
                       optionLabelArgDict,
                       badButtonnArgDict,
                       tamperedButtonArgDict,
                       duplicateButtonArgDict,
                       limitEntryArgDict,
                       limLabelArgDict,
                       stopButtonArgDict,
                       pauseButtonArgDict,
                       forwardButtonArgDict,
                       reverseButtonArgDict,
                       ScaleSLiderDict,
                       countLabelArgDict,
                       listFrameArgDict,
                       scrollbarArgDict,
                       listboxArgDict,
                       saveFrameArgDict,
                       saveButtonArgDict,
                       quitButtonArgDict]
        
        self.createWidgets(widgetSpecs)
        self.doBindings()
        self.finalizeFrames()
        self.setupScrollBar()
        self.goodCount = 0
        self.badCount  = 0
        self.tamperedCount = 0
        self.errorCount = 0
        self.speedControl = SpeedControl()
        self.updateCountLabel()
        self.afterID = None
        self.startPolling()
        self.labels2Go = 0
        self.paused    = False
        self.lastSN = None
        self.startSpeed = self.currentSpeed.get()
        self.sendCommand(0)
        #print(dir(self.widgetDict['countLabel']))
        #self.logMessage('polling started in constructor: ' +str(self.afterID))
        
    def  createWidgets(self,widgetSpecs):
        for dict in widgetSpecs:
            try:
                dict['argDict']['master'] = dict['argDict']['master']()
            except:
                pass
            self.widgetDict[dict['argDict']['name']] = eval(dict['widget'])( **dict['argDict'])
            self.widgetDict[dict['argDict']['name']].grid(**dict['gridDict'])
            try:
                dict['confFunc']()
            except:
                pass
                
    def doBindings(self):
        self.master.bind('<Alt-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-x>',     lambda *args : self.quit() )
        self.master.bind('<Control-q>', lambda *args : self.quit() )
        #self.widgetDict['countLabel'].bind( "<Button>", lambda *args :  self.resetCount())
        self.widgetDict['speedSlider'].bind('<ButtonRelease-1>', self.sendSpeedCommand)
        
    def finalizeFrames(self):
        colWeights = [50,1,1,1]
        rowWeights = [2,2,1,20,2]
        nbCols = len(colWeights)
        nbRows = len(rowWeights)
        optionFrameColWeights = [1,20]
        optionFrameRowWeights = [1,1,1]

        for i in range(len(optionFrameColWeights)):
            self.widgetDict['optionFrame'].columnconfigure(i,weight=optionFrameColWeights[i])
        for i in range(len(optionFrameRowWeights)):
            self.widgetDict['optionFrame'].rowconfigure(i,weight=optionFrameRowWeights[i])
        
        self.widgetDict['listFrame'].columnconfigure(0,weight=1)
        self.widgetDict['listFrame'].rowconfigure(0,weight=1)

        self.widgetDict['saveFrame'].rowconfigure(0, weight=1)
        self.widgetDict['saveFrame'].columnconfigure(0,weight=1)
        self.widgetDict['saveFrame'].columnconfigure(1,weight=1)

        for x in range(nbCols):
            self.master.columnconfigure( x, weight=colWeights[x])
        
        for y in range(nbRows):
            self.master.rowconfigure(y, weight=rowWeights[y])

    def setupScrollBar(self):
        self.widgetDict['scrollbar'].configure(command=self.widgetDict['listbox'].yview)
        self.widgetDict['listbox'].configure(yscrollcommand=self.widgetDict['scrollbar'].set)

    ##################### VERSION ####################################
            
    def showVersion(self,*args):
        messagebox.showinfo('System Version: ', versionString)
                            
    ##################### QUIT  ####################################
    def quit(self):
        self.sendCommand(0)
        time.sleep(0.1)
        super().quit()
        
    ################## Direction #####

    def directionCallBack(self,direction=0):
        dButVec = [[self.widgetDict['forwardButton'],'f','forwardColor','Forward'],
                   [self.widgetDict['reverseButton'],'r','reverseColor','Reverse']]
        triplet = dButVec[direction]
        command = triplet[1]
        commandText = triplet[3] 
        bText   = (commandText  if not direction else commandText[:-1]) + 'ing'
        if not self.isMoving():
            triplet[0].configure(text=bText,bg=activeColor)
            self.resetStopPause()
            self.clearInQ()
            #self.sendCommand(command)
            if not self.paused:
                self.labels2Go = self.currentLimit.get()
            self.paused = False
            print(self.labels2Go)
            self.startSpeed = self.currentSpeed.get()
            self.sendCommand(self.currentSpeed.get() if not direction else -self.currentSpeed.get())
            #self.sendCommand(100 if not direction else -100)

    def resetDirText(self ):
        self.widgetDict['forwardButton'].configure(text='Forward',bg=forwardColor)
        self.widgetDict['reverseButton'].configure(text='Reverse',bg=reverseColor)
        #print('GUI: polling stopeed in resetDirText ', self.afterID)
        #self.stopPolling()

    def isMoving(self):
        return 'ing' in self.widgetDict['forwardButton'].cget('text') + \
            self.widgetDict['reverseButton'].cget('text')
        
    ######################## stop pause #####
        
    def stopCallBack(self):
        stopButton = self.widgetDict['stopButton']
        pauseButton = self.widgetDict['pauseButton']
        if stopButton.cget('text') == 'Stop':
            #self.speedControl.reset()
            self.speedControl.reset(self.goodCount+self.badCount+self.tamperedCount)
            self.currentSpeed.set(self.startSpeed)
            self.sendCommand(0)
            self.paused=False
            stopButton.configure(text='Stopped',bg=activeColor)
            pauseButton.configure(text='Pause',bg=pauseColor)
            self.resetDirText()
            
    def pauseCallBack(self):
        stopButton = self.widgetDict['stopButton']
        pauseButton = self.widgetDict['pauseButton']
        if pauseButton.cget('text') == 'Pause' and \
           stopButton.cget('text') != 'Stopped':
            #self.sendCommand('p') #pauseButton.cget('text'))
            #self.speedControl.reset()
            self.speedControl.reset(self.goodCount+self.badCount+self.tamperedCount)
            self.sendCommand(0)
            self.paused = True
            self.lastSN = None
            stopButton.configure(text='Stop',bg=stopColor)
            pauseButton.configure(text='Paused',bg=activeColor)
            self.resetDirText()

    def resetStopPause(self):
        self.widgetDict['pauseButton'].configure(text='Pause',bg='yellow')
        self.widgetDict['stopButton'].configure(text='Stop',bg='red')
        Config.enableSpeedControl = True
        self.speedControl.reset(self.goodCount+self.badCount+self.tamperedCount)

    ######################### Label Limit  ############""

    def configureLimit(self):
        self.currentLimit.set(10)
        self.currentLimit.trace(mode ='w',
                                callback=self.testLim)
        self.widgetDict['limitEntry'].bind('<Return>', self.setLim )
            
    def testLim(self,*args):
        e = self.widgetDict['limitEntry']
        try:
            #self.sendCommand('l',str(currentLimit.get()))
            #e.configure(bg = entryColor)
            e.configure(bg = limitActiveColor)
        except:
            e.configure(bg = limitErrorColor)

    def setLim(self,*args):
        e = self.widgetDict['limitEntry']
        l = self.widgetDict['limitLabel']
        #print(int(str(self.currentLimit.get())))
        self.currentLimit.set(int(str(self.currentLimit.get())))
        try:   
            #self.sendCommand('l',str(self.currentLimit.get()))
            e.configure(bg = limitColor)
            l.configure(bg = limitColor)
        except:
            e.configure(bg = limitErrorColor)
            l.configure(bg = limitErrorColor)

    ######################## speedSlider ####################################

    def configureSpeed(self):
        self.currentSpeed.set(defaultStartSpeedPct)
        self.sendSpeedCommand()

    def sendSpeedCommand(self, unused=None):
        if self.isMoving():
            sign = 1 if 'ing' in self.widgetDict['forwardButton'].cget('text') else -1
            speed = self.currentSpeed.get() * sign
            self.sendCommand(speed)
        
    ######################### countLabel #####################################

    def resetCount(self):
        self.goodCount     = 0
        self.badCount      = 0
        self.tamperedCount = 0
        self.errorCount    = 0
        self.lastSN        = None
        self.restCounts    = False 
        self.updateCountLabel()
        self.speedControl.reset(self.goodCount+self.badCount+self.tamperedCount)

    def updateCountLabel(self):
        formatString = 'Label Count: {:>5d}  :  Good Labels: {:>5d}  :  Bad Labels: {:>5d}  :  Tampered: {:>5d}'
        self.countString.set(formatString.format(self.goodCount+self.badCount+self.tamperedCount,
                                                 self.goodCount,
                                                 self.badCount,
                                                 self.tamperedCount))
        self.widgetDict['countLabel'].configure(bg=getCountColor(self.goodCount,
                                                                 self.badCount))
        ## auto speed control
        if Config.enableSpeedControl and \
           self.speedControl.updateSpeedFactor(self.goodCount+self.badCount+self.tamperedCount):
            print(f'Speed Pct before correction : {self.currentSpeed.get()}')
            self.currentSpeed.set(round(self.currentSpeed.get()/self.speedControl.speedFactor))
            self.sendSpeedCommand()
            print(f'Speed Pct corrected : {self.currentSpeed.get()}')

    
    ######################### listbox #####################################

    def getFromQ(self):
        #print('GUI: polling from the gui...')
        #formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength) +'}' +'{:>8}{:>8}'
        formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength) +'}' +'{:>8}'
        try:
            item = self.inQ.get(False)
            self.inQ.task_done()
        except queue.Empty:
            item = None
        if item:
            self.labels2Go -= 1
            print(self.labels2Go)
            self.doSlowDown()
            """
            debugMsg = '** GUI Display Label\n* got this from the inQ: <start>' +str(item) +'<end>' 
            #self.logMessage(debugMsg)
            debugMsg = '** GUI inQ size:  <start>' +str(self.inQ.qsize()) + '<end>' 
            #self.logMessage(debugMsg)
            """
            listbox = self.widgetDict['listbox']
            listbox.insert(tk.END,  formatString.format(int(item[0]),str(item[1]),str(item[2])))
            self.goodOrBadLabelLoad(listbox,item)
            self.lastSN =str(item[1])
            listbox.yview(tk.END)
        if self.labels2Go == 0:  #item == '0': #int(item[0]) == 0:
            #reached limit
            self.stopCallBack() # automatic stop
            debugMsg = '** GUI inQ size:  <start>' +str(self.inQ.qsize()) + '<end>' 
            #self.logMessage(debugMsg)
        self.startPolling()

    def doSlowDown(self):
        if self.labels2Go <= Config.slowdownLimit and \
           self.currentSpeed.get() > Config.maxSpeedPCTAtStop:
            Config.enableSpeedControl = False
            print('Slowing down...')
            self.currentSpeed.set(max(Config.maxSpeedPCTAtStop, round(self.currentSpeed.get()*Config.stopSlowdownFactor)))
            self.sendSpeedCommand()
        
    def goodOrBadLabelLoad(self,listbox,item):
        col = listColor
        #formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength) +'}' +'{:>8}{:>8}'
        formatString = '{:5d}{:>' + '{:d}'.format(maxSNLength) +'}' +'{:>16}'

        if item[1] in badLabelStringVec or str(item[1]) == self.lastSN:  # it's a bad boy
            col = badTagColor
            #item[3]= DuplicateIndicator if str(item[1]) == self.lastSN else BadIndicator
            if str(item[1]) == self.lastSN:
                item[2]= DuplicateIndicator
                if self.duplicatePause.get():
                    self.pauseCallBack()
            else:
                item[2]=  BadIndicator
                if self.badPause.get():
                    self.pauseCallBack()
            listbox.delete(tk.END)
            listbox.insert(tk.END,  formatString.format(int(item[0]),str(item[1]),str(item[2])))
            self.badCount +=1
        elif item[2] in badLabelStringVec[1:]:  # it's tampered data
            col = tamperedTagColor
            item[2]= TamperedIndicator
            if self.tamperedPause.get():
                self.pauseCallBack()
            listbox.delete(tk.END)
            listbox.insert(tk.END,  formatString.format(int(item[0]),str(item[1]),str(item[2])))
            self.tamperedCount += 1
        else:
            listbox.delete(tk.END)
            listbox.insert(tk.END,  formatString.format(int(item[0]),str(item[1]),''))
            self.goodCount += 1
        listbox.itemconfig(tk.END, background = col)
        self.updateCountLabel()
        #if self.speedControl.currentSpeed!=0:
        #    listbox.after(1000*Config.stopAfterNbLabels/self.speedControl.currentSpeed, lambda: self.stopCallBack(self))

    def startPolling(self):
        self.afterID = self.after(listBoxPollDelay,self.getFromQ)

    def stopPolling(self):
        #self.logMessage('polling canceled on ' + str(self.afterID))
        self.after_cancel(self.afterID)

        
    ############################### SAVE FILE ################################

    def saveFile(self):
        data = [('All tyes(*.*)', '*.*')]
        file = filedialog.asksaveasfile(mode='w', defaultextension=".csv")
        if file:
            headers = ('Sequence','Serial_Number','Status') #,'NbReads')
            #headers = ('Data',)
            #writer = csv.writer(file, delimiter='',
            writer = csv.writer(file,delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(headers)
            for line in self.widgetDict['listbox'].get(0,tk.END):
                lineSplit = line.strip().split()
                if len(lineSplit)<3:
                    lineSplit.append("Ok")
                #print(lineSplit,len(lineSplit))
                writer.writerow([lineSplit[0],lineSplit[1],lineSplit[2]]) #,lineSplit[3]])
                #self.logMessage(lineSplit)
                #writer.writerow([line]) 
            file.close()
            messagebox.showinfo('Created File: ',file.name)

    ############################# SEND COMMAND ###############################
    
            
    def sendCommand(self,com, arg = None):
        #return
        if com == 0:
            overRunSecs =  (10*Config.millisPauseBeforStop)/(self.currentSpeed.get()*1000.)
            #print('Over Run Time (ms)  :', overRunSecs * 1000)
            time.sleep(overRunSecs)
        msg = 'User Command : '+ str(com) +' ' + str(arg)
        self.logMessage(msg)
        self.outQ.put(com)

    def clearInQ(self):
        good = True
        while good:
            try:
                item = self.inQ.get(False)
                self.inQ.task_done()
                self.logMessage('Q cleared of this: '+ str(item))
                time.sleep(0.1)
            except queue.Empty:
                good = False

    def logMessage(self,s):
        #print(s)
        pass

def startup():
    outQ    = JoinableQueue()
    guiInQ  = JoinableQueue()
    guiOutQ = JoinableQueue()
    guiG = TaggyGui(outQ,guiInQ,guiOutQ)
    guiG.mainloop()
    
if __name__ == '__main__':
    startup()
