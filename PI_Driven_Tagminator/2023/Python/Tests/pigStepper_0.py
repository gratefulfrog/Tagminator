#!/usr/bin/python3

###############################
#### Conf
###############################

# pins are all GPIO numbers
stepPin   = 24
dirPin    = 25
enablePin = 5
enableDir = 0 # 1 means enabled when HIGH, O means enabled when LOW

minPulseUS = 10
minPauseUS = 1500
pulseDir   = 1  # 1 means pulse HIGH, O means pulse LOW

DEBUG = True

###############################
#### end of Conf
###############################

import sys
import time
import pigpio

def micros():
    return 1000*time.perf_counter_ns()
def nanos():
    return time.perf_counter_ns()
"""
def delayMicroseconds(pulseWidth):
    pw = 1000*pulseWidth
    now = nanos()
    while (nanos()-now < pw):
        pass
"""
def delayMicroseconds(pig, us):
    now = pig.get_current_tick()
    while pigpio.tickDiff(now, pig.get_current_tick()) < us:
        pass
    
class PigStepper(object):

    def __init__(self,
                 pi   = None,
                 sPin = stepPin,
                 dPin = dirPin,
                 ePin = enablePin,
                 eDir = enableDir,
                 minPulse = minPulseUS,
                 minPause = minPauseUS,
                 pulseD   = pulseDir):
        self.debugFunc = lambda x:x
        if DEBUG:
            self.debugFunc = print
        self.pig = pi
        if not pi:
            print('getting pig')
            self.pig = pigpio.pi()
            if not self.pig.connected:
                print('PIGIO connection failed!')
                sys.exit(0)
            print('got pig')    
        time.sleep(2)
        self.stepPin   = sPin
        self.pig.set_mode(self.stepPin,pigpio.OUTPUT)
        self.dirPin    = dPin  # 1 is forward, 0 is reverse
        self.pig.set_mode(self.dirPin,pigpio.OUTPUT)
        self.setDirection(True)
        self.enablePin = ePin
        self.pig.set_mode(self.enablePin,pigpio.OUTPUT)
        self.enableDir = eDir
        self.speed     = 0  # is a percent of 100% min is 1%, below that is 0
        self.setPulseLen(minPulse)
        self.pauseUS   = minPause
        self.pulseDir  = pulseD
            
    def __str__(self):
        res  = f'Pig                 : {self.pig}\n'
        res += f'Step Pin,val,mode   : {self.stepPin}, {self.pig.read(self.stepPin)}, {"OUTPUT" if self.pig.get_mode(self.stepPin) else "INPUT"}\n'
        res += f'Dir Pin,val,mode    : {self.dirPin}, {self.getDirection()}, {"OUTPUT" if self.pig.get_mode(self.dirPin) else "INPUT"}\n'
        res += f'Enable Pin,val,mode : {self.enablePin}, {self.isEnabled()}, {"OUTPUT" if self.pig.get_mode(self.enablePin) else "INPUT"}\n'
        res += f'Enable Dir          : {self.enableDir}\n'
        res += f'Speed               : {self.speed}\n'
        res += f'Pulse Length        : {self.pulseUS}\n'
        res += f'Pulse Dir           : {self.pulseDir}\n'
        res += f'Pause Length        : {self.pauseUS}'
        return res
        
    def setPulseLen(self,plUS):
        self.pulseUS   = plUS
        self.pulseFunc = self.pulsePinTrigger if plUS <= 100 else self.pulsePinWrite
        
    def pulsePinTrigger(self):
        #print('call pulse pin trigger')
        self.pig.gpio_trigger(self.stepPin,self.pulseUS,self.pulseDir)
        #delayMicroseconds(self.pauseUS)
        delayMicroseconds(self.pig,self.pauseUS)
        
    def pulsePinWrite(self):
        self.pig.write(self.stepPin,self.pulseDir)
        #delayMicroseconds(self.pulseUS)
        delayMicroseconds(self.pig,self.pulseUS)
        self.pig.write(self.stepPin, not self.pulseDir)
        #delayMicroseconds(self.pauseUS)
        delayMicroseconds(self.pig,self.pauseUS)
        
    def setSpeed(self, speedPCT):
        if speedPCT < 1:
            self.speed = 0
            self.enable(False)
        else:
            self.speed = speedPCT
            self.pauseUS = minPauseUS*100/speedPCT
        self.debugFunc(f'setSpeed({speedPCT} pauseUS: {self.pauseUS}')

    def getSpeed(self):
        return self.speed

    def isEnabled(self):
        return self.pig.read(self.enablePin) == self.enableDir

    def enable(self,yes):
        self.pig.write(self.enablePin, self.enableDir if yes else not self.enableDir)

    def step(self):
        #print('one step')
        if self.speed > 0:
            #print('call pulsefunc')
            self.pulseFunc()

    def getDirection(self):
        return self.pig.read(self.dirPin)

    def setDirection(self,d):
        self.pig.write(self.dirPin,d)
        self.debugFunc(f'setDirection({d}) dirPin: {self.pig.read(self.dirPin)}')
        
if __name__ == '__main__':
    pStepper = PigStepper()
    stepsPerRev  = 8000
    pStepper.setSpeed(100)

    if len(sys.argv) >1:
        stepsPerRev = int(sys.argv[1])
    if len(sys.argv) >2:
        pStepper.setSpeed(int(sys.argv[2]))
        
    print(pStepper)
    print(f'\nSteps/Rev    : {stepsPerRev}')
    time.sleep(1)

    pStepper.enable(True)
    try:
        minStepsSec = 100000
        maxStepsSec = 0

        while True:
            now = time.time()
            pStepper.enable(True)
            for i in range(stepsPerRev):
                pStepper.step()
            pStepper.enable(False)
            stepsSec = stepsPerRev/(time.time()-now)
            minStepsSec = min(minStepsSec,stepsSec)
            maxStepsSec = max(maxStepsSec,stepsSec)
            print(round(stepsSec),round(minStepsSec),round(maxStepsSec), 'current, min, max steps/sec')
            pStepper.setDirection(not pStepper.getDirection())
            time.sleep(1)
    except KeyboardInterrupt:
        print()
    finally:
        print(pStepper)
        pStepper.enable(False)

