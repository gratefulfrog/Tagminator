
#######################################
# Copyright (c) 2021 Maker Portal LLC
# Author: Joshua Hrisko
#######################################
#
# NEMA 17 (17HS4023) Raspberry Pi Tests
# --- rotating the NEMA 17 to test
# --- wiring and motor functionality
#
#
#######################################
#
import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib
import time
import sys

################################
# RPi and Motor Pre-allocations
################################
#
#define GPIO pins
direction= 22 # Direction (DIR) GPIO Pin
step = 23 # Step GPIO Pin
EN_pin = 24 # enable pin (LOW to enable)

# Declare a instance of class pass GPIO pins numbers and the motor type

###########################
# Actual motor control
###########################
#
def oneTurn(delMS,steps=2038,direc = True):
    #mymotortest = RpiMotorLib.A4988Nema(direction, step, (21,21,21), "DRV8825")
    mymotortest = RpiMotorLib.A4988Nema(direction, step, (-1,-1,-1), "A4988")

    #GPIO.setup(EN_pin,GPIO.OUT) # set enable pin as output
    #GPIO.output(EN_pin,GPIO.LOW) # pull enable to low to enable motor
    try:
        now = time.time()
        mymotortest.motor_go(direc, # True=Clockwise, False=Counter-Clockwise
                             "Full" , # Step type (Full,Half,1/4,1/8,1/16,1/32)
                             steps, # number of steps
                             0.000001*delMS, # step delay [sec]
                             False, # True = print verbose output 
                             0) # initial delay [sec]
        print(time.time()-now)
    except:
        sys.exit(0)
    finally:
        GPIO.cleanup() # clear GPIO allocations after run
