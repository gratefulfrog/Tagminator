#!/usr/bin/python3

import signal
import sys
import RPi.GPIO as GPIO

BUTTON_GPIO = 4

def signal_handler(sig, frame):
    GPIO.cleanup()
    print('\nclean exit..')
    sys.exit(0)

def button_pressed_callback(channel):
    print("Label Start!")

if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_GPIO, GPIO.IN) #, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(BUTTON_GPIO, GPIO.RISING, 
            callback=button_pressed_callback, bouncetime=10)
    
    signal.signal(signal.SIGINT, signal_handler)
    signal.pause()
