
BobConf = False #True

class Config():
    #### TAG READER Parameters ################
    TagReaderBaud    = 9600
    TagReaderPort    = '/dev/ttyACM0'

    #### Label Detector Parameters ################
    LabelDetectedPin = 4  # GPIO numbering

    #### Stepper Driver Parameters ################
    StepPin          = 24 # GPIO numbering
    DirectionPin     = 25 # GPIO numbering
    EnablePin        = 5  # GPIO numbering
    EnableDirection = 0
    # 1 means enabled when HIGH, O means enabled when LOW
    MinPulseUS       = 10
    MinPauseUS       = 1000 if BobConf else 40 #1000
    PulseDirection   = 1  # 1 means pulse HIGH, O means pulse LOW

    #### Relay Parameters ################
    RelayPin         = 27 # GPIO numbering
    InvertRelayDirection = True
    # if true relay pin will get opposite value of direcition pin

    #### Speed and Stopping Parameters ################
    initialStartSpeedPct  = 20
    millisPauseBeforStop  = 100
    enableSpeedControl   = True
    slowdownLimit        = 3   # labels from end when the decelleration starts
    maxSpeedPCTAtStop    = 10    # percent speed to let it go
    stopSlowdownFactor   = 0.6  # multiplies current speed to slow down
    speedMeasurementTime  = 5 # seconds

    #### Debug Parameters ################
    Debug            = True
    
    
