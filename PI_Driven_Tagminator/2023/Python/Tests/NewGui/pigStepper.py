#!/usr/bin/python3


###############################
#### end of Conf
###############################

import sys
import time
import pigpio

from config import Config

###############################
#### Conf do not edit here, use the config.py module only!!
###############################

# pins are all GPIO numbers
stepPin   = Config.StepPin
dirPin    = Config.DirectionPin
enablePin = Config.EnablePin
enableDir = Config.EnableDirection # 1 means enabled when HIGH,
                                   # O means enabled when LOW
relayPin = Config.RelayPin
invertRelayDir = Config.InvertRelayDirection

minPulseUS = Config.MinPulseUS
minPauseUS = Config.MinPauseUS
pulseDir   = Config.PulseDirection  # 1 means pulse HIGH, O means pulse LOW

DEBUG = False #Config.Debug

def micros():
    return 1000*time.perf_counter_ns()
def nanos():
    return time.perf_counter_ns()

def delayMicroseconds(pig, us):
    now = pig.get_current_tick()
    while pigpio.tickDiff(now, pig.get_current_tick()) < us:
        pass
    
class PigStepper(object):

    def __init__(self,
                 pi   = None,
                 sPin = stepPin,
                 dPin = dirPin,
                 ePin = enablePin,
                 eDir = enableDir,
                 rPin = relayPin,
                 iRelay    = invertRelayDir,
                 minPulse  = minPulseUS,
                 minPause  = minPauseUS,
                 pulseD    = pulseDir):
        self.debugFunc = lambda x:x
        if DEBUG:
            self.debugFunc = print
        self.pig = pi
        if not pi:
            print('getting pig')
            self.pig = pigpio.pi()
            if not self.pig.connected:
                print('PIGIO connection failed!')
                sys.exit(0)
            print('got pig')    
        time.sleep(2)
        self.stepPin   = sPin
        self.pig.set_mode(self.stepPin,pigpio.OUTPUT)
        self.dirPin    = dPin  # 1 is forward, 0 is reverse
        self.pig.set_mode(self.dirPin,pigpio.OUTPUT)
        self.enablePin = ePin
        self.pig.set_mode(self.enablePin,pigpio.OUTPUT)
        self.enableDir = eDir
        self.relayPin  = rPin
        self.pig.set_mode(self.relayPin,pigpio.OUTPUT)
        self.invertRelay = iRelay

        self.setDirection(True)
        self.speed     = 0  # is a percent of 100% min is 1%, below that is 0
        self.setPulseLen(minPulse)
        self.pauseUS   = minPause
        self.pulseDir  = pulseD
        self.running   = False
        self.waveID    = None
            
    def __str__(self):
        res  = f'Pig                 : {self.pig}\n'
        res += f'Enable Pin,val,mode : {self.enablePin}, {self.isEnabled()}, {"OUTPUT" if self.pig.get_mode(self.enablePin) else "INPUT"}\n'
        res += f'Step Pin,val,mode   : {self.stepPin}, {self.pig.read(self.stepPin)}, {"OUTPUT" if self.pig.get_mode(self.stepPin) else "INPUT"}\n'
        res += f'Dir Pin,val,mode    : {self.dirPin}, {self.getDirection()}, {"OUTPUT" if self.pig.get_mode(self.dirPin) else "INPUT"}\n'
        res += f'Relay Pin,val,mode  : {self.relayPin}, {self.pig.read(self.relayPin)}, {"OUTPUT" if self.pig.get_mode(self.relayPin) else "INPUT"}\n'
        res += f'\nEnable Dir          : {self.enableDir}\n'
        res += f'Invert Relay        : {self.invertRelay}\n'
        res += f'Speed               : {self.speed}\n'
        res += f'Pulse Length        : {self.pulseUS}\n'
        res += f'Pulse Dir           : {self.pulseDir}\n'
        res += f'Pause Length        : {self.pauseUS}\n'
        res += f'Running             : {self.running}\n'
        res += f'Wave ID             : {self.waveID}'
        return res
        
    def setPulseLen(self,plUS):
        self.pulseUS   = plUS
        self.pulseFunc = self.pulsePinTrigger if plUS <= 100 else self.pulsePinWrite
        
    def pulsePinTrigger(self):
        #print('call pulse pin trigger')
        self.pig.gpio_trigger(self.stepPin,self.pulseUS,self.pulseDir)
        #delayMicroseconds(self.pauseUS)
        delayMicroseconds(self.pig,self.pauseUS)
        
    def pulsePinWrite(self):
        self.pig.write(self.stepPin,self.pulseDir)
        #delayMicroseconds(self.pulseUS)
        delayMicroseconds(self.pig,self.pulseUS)
        self.pig.write(self.stepPin, not self.pulseDir)
        #delayMicroseconds(self.pauseUS)
        delayMicroseconds(self.pig,self.pauseUS)
        
    def setSpeed(self, speedPCT):
        if speedPCT < 1:
            self.speed = 0
            self.enable(False)
            self.stop()
        else:
            self.running = self.isEnabled()
            self.speed = speedPCT
            self.pauseUS = round(minPauseUS*100/speedPCT)
            self.getWave()
            self.pig.wave_send_repeat(self.waveID)
        self.debugFunc(f'setSpeed({"%03d"%speedPCT}%)      : {self.pauseUS} (pauseUS)')

    def getWave(self):
        waveLis = []
        waveLis.append(pigpio.pulse(1<<self.stepPin, 0 ,              self.pulseUS))   
        waveLis.append(pigpio.pulse(0,               1<<self.stepPin, self.pauseUS))

        self.pig.wave_clear() # clear any existing waveforms
        self.pig.wave_add_generic(waveLis) 
        self.waveID  = self.pig.wave_create() # create and save id
        
    def getSpeed(self):
        return self.speed

    def isEnabled(self):
        return self.pig.read(self.enablePin) == self.enableDir

    def enable(self,yes):
        self.pig.write(self.enablePin, self.enableDir if yes else not self.enableDir)

    def step(self):
        #print('one step')
        if self.speed > 0:
            self.pig.wave_send_once(self.waveID)

    def runSpeed(self):
        self.running = True
        self.pig.wave_send_repeat(self.waveID)

    def stop(self):
        self.pig.wave_tx_stop()
        self.pig.wave_clear()
        self.running = False

    def runSpeeding(self):
        return self.running
    
    def getDirection(self):
        return self.pig.read(self.dirPin)

    def setDirection(self,d):
        self.pig.write(self.dirPin,d)
        self.pig.write(self.relayPin, d^self.invertRelay)
        self.debugFunc(f'setDirection({1 if d else 0})     : dirPin: {self.pig.read(self.dirPin)}')
        
if __name__ == '__main__':
    pStepper = PigStepper()
    stepsPerRev  = 8000
    pStepper.setSpeed(100)

    if len(sys.argv) >1:
        stepsPerRev = int(sys.argv[1])
    if len(sys.argv) >2:
        pStepper.setSpeed(int(sys.argv[2]))
        
    print(pStepper)
    print(f'\nSteps/Rev    : {stepsPerRev}')
    time.sleep(1)

    pStepper.enable(True)
    try:
        minStepsSec = 100000
        maxStepsSec = 0
        pStepper.enable(True)
        pStepper.setSpeed(100) #runSpeed()
        while True:
            pass
        """
        while True:
            now = time.time()
            pStepper.enable(True)
            for i in range(stepsPerRev):
                pStepper.step()
                time.sleep(0.1)
            pStepper.enable(False)
            stepsSec = stepsPerRev/(time.time()-now)
            minStepsSec = min(minStepsSec,stepsSec)
            maxStepsSec = max(maxStepsSec,stepsSec)
            print(round(stepsSec),round(minStepsSec),round(maxStepsSec), 'current, min, max steps/sec')
            pStepper.setDirection(not pStepper.getDirection())
            time.sleep(1)
        """
    except KeyboardInterrupt:
        print()
    finally:
        print(pStepper)
        pStepper.stop()
        pStepper.enable(False)

