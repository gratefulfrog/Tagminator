#!/usr/bin/python3

DETECTOR_PIN        = 4

from time import time,sleep
from multiprocessing import Process, JoinableQueue
import sys
import pigpio
import signal

class LabelDetectorSerialServer(Process):
    """
    Implent the serving of the interrupts from the label detector
    """
    #    def __init__(self, dataOutQ, inputPin):
    def __init__(self, piggy, inputPin):
        Process.__init__(self,daemon=True)
        self.pig = piggy
        self.count = 0
        self.header = 'Label: '
        self.msg = ''
        self.pig.set_mode(inputPin, pigpio.INPUT)
        self.pig.set_pull_up_down(inputPin, pigpio.PUD_DOWN)
        self.pigCallback = self.pig.callback(inputPin, pigpio.RISING_EDGE,self.labelDetectHandler)
        signal.signal(signal.SIGINT, self.signal_handler)
        
    def labelDetectHandler(self,gpio, level, tick):
        self.msg = self.header + str(self.count)
        print(self.msg)
        self.count+=1

    def signal_handler(self, sig, frame):
        self.pig.stop()
        print('\nclean exit..')
        sys.exit(0)
            
    def run(self):
        """ version for input pin interrurp testing
        """
        signal.pause()


def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      sys.exit()
   return pig

def startup():
    p = getPig()
    labelDetector = LabelDetectorSerialServer(p,DETECTOR_PIN) 
    labelDetector.start()

if __name__ == '__main__':
    startup()
    signal.pause()
