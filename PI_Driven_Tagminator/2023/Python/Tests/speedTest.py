#!/usr/bin/python3

import time 
import sys
import signal

minT = 2  # seconds

class SpeedControl():
    def __init__(self,initialLabelCount=0):
        self.deltaMin    = minT # in seconds
        self.reset(initialLabelCount)
        
    def reset(self,initialLabelCount = 0):
        self.lastCount     = initialLabelCount
        self.speedFactor   = 1
        self.lastSpeedTime = 0
        self.initialSpeed  = 0
        
    def updateSpeedFactor(self,currentCount):
        if not self.lastSpeedTime:
            self.lastSpeedTime = time.time()

        now = time.time()
        deltaT = now - self.lastSpeedTime
        if (deltaT >= minT):
            deltaCount = currentCount - self.lastCount
            if not self.initialSpeed:
                self.initialSpeed = deltaCount/deltaT
            self.lastCount  = currentCount
            self.lastSpeedTime = now
            currentSpeed = deltaCount/deltaT   #labels per second
            self.speedFactor = max(1,round(currentSpeed/self.initialSpeed))
            self.showSpeedFactor(deltaCount,currentSpeed)

    def showSpeedFactor(self,deltaCount,currentSpeed):
        print(f'delta Count      : {deltaCount}')
        print(f'New Speed Factor : {self.speedFactor}')
        print(f'Current Speed    : {currentSpeed}')
        print(f'New Speed        : {currentSpeed/self.speedFactor}')
        

def signal_handler(sig, frame):
    print('\nclean exit..')
    sys.exit(0)
                        
def startup():
    signal.signal(signal.SIGINT, signal_handler)
    inst = SpeedControl()
    count = 1
    while True:
        inst.updateSpeedFactor(count)
        time.sleep(1)
        count *=2

if __name__ == '__main__':
    startup()


