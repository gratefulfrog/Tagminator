#!/usr/bin/python3

from time import time,sleep
from multiprocessing import Process, JoinableQueue
import queue
import sys
import pigStepper


class PigServer(Process):
    """
    test of pigstepping
    """
    def __init__(self, dataQ):
        Process.__init__(self,daemon=True)
        self.Q          = dataQ
        self.pigStepper = pigStepper.PigStepper()
    
    def run(self):
        """ version for testing
        """
        try:
            while True:
                self.pigStepper.step()
                try:
                    item = self.Q.get(False)
                except queue.Empty:
                    pass
                else:
                    self.Q.task_done()
                    self.processIncoming(int(item)) # trailing line feed or cr
        except KeyboardInterrupt:
            self.pigStepper.enable(False)
            print('stepper disabled!')
            sys.exit(0)
            
    def processIncoming(self,nb):
        self.pigStepper.setSpeed(abs(nb))
        self.pigStepper.setDirection(nb>=0)
        self.pigStepper.enable(nb!=0)
        print(self.pigStepper)
                
def startup():
    rt = 2
    speed = 100
    if len(sys.argv) >1:
        rt = int(sys.argv[1])
    if len(sys.argv) >2:
        speed = int(sys.argv[2])
    
    Q         = JoinableQueue()
    pigServer = PigServer(Q)
    pigServer.start()
    comV =[speed,0,-speed,0]
    try:
        while True:
            for c in comV:
                Q.put(c)  # just put a positive or negative speed on the q
                          # or zero to stop the stepper
                sleep(rt)
    except KeyboardInterrupt:
        pass
    finally:
        print('\nClean main exit')
        sys.exit(0)

if __name__ == '__main__':
    startup()
    sys.exit(0)
