#!/usr/bin/python3

import tkinter as tk
from os import getcwd

speedColor      = 'pink'
speedTroughColor= 'blue'

activeColor     = 'blue'
forwardColor    = 'green'
reverseColor    = 'cyan4'
entryColor      = 'white'
limitColor      = 'SeaGreen1'
limitActiveColor= 'cornsilk3'
limitErrorColor = 'red'
stopColor       = 'red'
pauseColor      = 'yellow'
listColor       = 'gray80'
badTagColor     = 'red'
doubleSNColor   = 'orange2'
saveColor       = 'cornsilk3'
quitColor       = 'cornsilk4'
countColor      = ['sea green',       # 0
                   'PaleGreen4',      # 1
                   'PaleGreen3',      # 2
                   'PaleGreen2',      # 3
                   'PaleGreen1',      # 4
                   'goldenrod1',      # 5
                   'goldenrod2',      # 6
                   'goldenrod3',      # 7
                   'goldenrod4',      # 8
                   'firebrick1',      # 9
                   'firebrick2',      # 10
                   'firebrick3',      # 11
                   'firebrick4',      # 12
                   'orange red']      # 13


guiVersionString = 'Taggy Gui: ' + getcwd().split('/')[-2]

class TaggyGui(tk.Frame):
    commandDict = {'l' : '0', # followed by argument as a string!
                   'f' : '1',
                   'r' : '2',
                   's' : '3',
                   'p' : '4',
                   'v' :  None,  # not implemented
                   'h' :  None,  # not implemented
                   'g' : 6} # followed by speed argument as string (not signed?)
    def __init__(self,master=None):
        tk.Frame.__init__(self, master)
        self.master.title("Tagminator")
        self.master.geometry('1000x800')                
        self.grid()
        self.currentLimit = tk.IntVar()
        self.currentSpeed = tk.IntVar()
        self.countString  = tk.StringVar()
        self.widgetDict = {}

        ScaleSLiderDict = {  'widget'  : 'tk.Scale',
                             'argDict' : {'master'       : self.master,
                                          'name'         : 'speedSlider',
                                          'bg'           : speedColor,
                                          'troughcolor'  : speedTroughColor,
                                          'label'        : 'Speed (%)',
                                          'sliderrelief' : 'raised',
                                          'relief'       : 'sunken',
                                          'from_'        : 1,
                                          'to'           : 100,
                                          'orient'       : tk.HORIZONTAL,
                                          'length'       : 350,
                                          'showvalue'    : 1,
                                          #'tickinterval' : 2,
                                          'resolution'   : 1,
                                          'variable'     : self.currentSpeed},
                             #'command'      : self.setSpeed},
                             'gridDict' : {'row'    : 0,
                                           'column' : 0,
                                           'sticky' : 'NSEW'},
                             'confFunc' : self.configureSpeed}
        widgetSpecs = [ScaleSLiderDict]
        
        self.createWidgets(widgetSpecs)
        #self.configureSpeed()
        
    def  createWidgets(self,widgetSpecs):
        for dict in widgetSpecs:
            try:
                dict['argDict']['master'] = dict['argDict']['master']()
            except:
                pass
            self.widgetDict[dict['argDict']['name']] = eval(dict['widget'])( **dict['argDict'])
            self.widgetDict[dict['argDict']['name']].grid(**dict['gridDict'])
            try:
                dict['confFunc']()
            except:
                pass

    def configureSpeed(self):
        self.currentSpeed.set(40)
        #        self.widgetDict['speedSlider'].set(self.currentSpeed.get())
        self.widgetDict['speedSlider'].bind('<ButtonRelease-1>', self.sendSpeedCommand)
        self.sendSpeedCommand()

    def sendSpeedCommand(self,unused=None):
        print('updating speed: ', self.currentSpeed.get())
        print(guiVersionString)
                        

def runIt():
    app = TaggyGui()
    app.mainloop()


if __name__ == '__main__':
    runIt()
