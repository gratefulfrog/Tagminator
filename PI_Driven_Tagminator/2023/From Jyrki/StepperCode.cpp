//step controller
int dirPin = 7;
int pulPin = 8;
int enblPin = 6;
int motorSelectPin = 5;
int dir = 0; //0 = Rewind, 1 = Forward
double steps = 0;
int total = 15600;
double sspeed = 550; //the smaller the faster. Reliable but very slow setting is 1150
int rounds = 0;
int totalRounds = 2600;
char incomingByte;   // for incoming serial data

void setup(){
  Serial.begin(115200);
  pinMode(dirPin, OUTPUT);
  pinMode(pulPin, OUTPUT);
  pinMode(enblPin, OUTPUT);
  pinMode(motorSelectPin, OUTPUT);
  digitalWrite(enblPin, HIGH);
  Serial.println ("0 REWIND  - 1 FORWARD");     // signal initalization done
  while(Serial.available() == 0){}
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    // say what you got:
    Serial.print("I received: ");       
    Serial.println(incomingByte);    // just to prove it's being received
  }        
  if (incomingByte == '1'){
    dir = 1;
    Serial.println ("RUNNING FORWARD");
  }
  else if (incomingByte == '0') {
    dir = 0;
    Serial.println ("REWINDING");
  } 
}


void loop(){
  //Serial.print("run code");
  if (dir == 1){
    digitalWrite(dirPin, LOW);
    digitalWrite(motorSelectPin, HIGH);
  }
  if (dir == 0){
    digitalWrite(dirPin, HIGH);
    digitalWrite(motorSelectPin, LOW);
  }
  delay(1000);
  while (rounds < totalRounds){
    digitalWrite(enblPin, LOW);
    while (steps < total){
      digitalWrite(pulPin, HIGH);
      if(rounds==0){
        if(steps<=1000){
          sspeed=sspeed-(steps/2500);
          //Serial.print("speedup:");
          //Serial.println(sspeed);
        }
      }
      if(rounds==(totalRounds-1)){
        if(steps>=4600){
          sspeed=sspeed+0.01;
          //Serial.print("slowdown:");
	  //Serial.println(sspeed);
        }
      }
      delayMicroseconds(sspeed);      
      digitalWrite(pulPin, LOW);
      delayMicroseconds(10);
      steps+=1; 
    }
    steps = 0;
    rounds+=1;
    Serial.println(rounds);    
  }
  delay(1000);
  digitalWrite(enblPin, HIGH);
  while (rounds == totalRounds){}
}

