#include "config.h"

int counter = 0;

void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

void doPins(int val){
  int setting = val ? 1 : 0;
  digitalWrite(PIN_OUTPUT,setting);
  digitalWrite(LED_BUILTIN,setting);
}

void doMessage(int detection, int count){
  String msg = "";
  switch(detection){
    case LABEL_INIT:
      msg = String(STR_INIT);
      break;
    case LABEL_DETECTED:
      msg = String(STR_LABEL);
      msg.concat(count);
      break;
    case LABEL_UNDETECTED:
      msg = String(STR_NO_LABEL);
      msg.concat(count);
      break;
  }
  Serial.println(msg);
}

void labelInit(){
  doPins(LABEL_UNDETECTED);
  doMessage(LABEL_INIT, counter);
}

void labelDetected(){
  doPins(LABEL_DETECTED);
  //doMessage(LABEL_DETECTED, counter);
}

void labelUnDetected(){
  doPins(LABEL_UNDETECTED);
  doMessage(LABEL_UNDETECTED, counter++);
}

void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(PIN_OUTPUT,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  labelInit();
}
int lbsPerSec = 10;

void loop() {
  for (int i=0;i<lbsPerSec;i++){
    labelDetected();
    delayMS(1000/lbsPerSec);
  }
  labelUnDetected();
  delayMS(1000/lbsPerSec);
}
