#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

// Coms
#define BAUD_RATE (9600)

// PINS
#define PIN_INPUT (2)  // label detector pin
#define PIN_OUTPUT (3)  // ready pin

// Control
#define LABEL_DETECTED   (1)
#define LABEL_UNDETECTED (0)
#define LABEL_INIT       (-1)

// Strings
#define SN_LENGTH            (16)
#define TAMPERED_LENGTH      (8)
#define I_COUNTER_LENGTH     (3)
#define STR_I_COUNTER_FORMAT ("%03d")
#define STR_SN_PREFIX        ('E')
#define STR_SN_FORMAT        ("%c%015X")
#define TAMPERED             (0X8088)
#define NOT_TAMPERED         (0X88)
#define STR_TAMPERED_FORMAT  ("%08X")


#endif
