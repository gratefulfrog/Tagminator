#include "config.h"

int counter = 0;
const int milliSecBetweenLabels = 15; // 30;

void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

void doPins(int val){
  int setting = val ? 1 : 0;
  digitalWrite(PIN_OUTPUT,setting);
  digitalWrite(LED_BUILTIN,setting);
}

void labelInit(){
  doPins(LABEL_UNDETECTED);
}

void labelDetected(){
  doPins(LABEL_DETECTED);
}

void labelUnDetected(){
  doPins(LABEL_UNDETECTED);
}

void setup() {
  pinMode(PIN_OUTPUT,OUTPUT);
  pinMode(PIN_INPUT,INPUT_PULLUP);
  pinMode(PIN_ENABLE_INPUT,INPUT_PULLUP);
  pinMode(LED_BUILTIN,OUTPUT);
  labelInit();
}

void loop() {
  if(!digitalRead(PIN_INPUT) && !digitalRead(PIN_ENABLE_INPUT)){
    labelUnDetected();
    delayMS(milliSecBetweenLabels ); 
    labelDetected();
    delayMS(8* milliSecBetweenLabels);
  }
}
