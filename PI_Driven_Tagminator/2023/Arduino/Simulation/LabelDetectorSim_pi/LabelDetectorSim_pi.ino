#include "config.h"

int counter = 0;
const int milliSecBetweenLabels = 30;

void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

void doPins(int val){
  int setting = val ? 1 : 0;
  digitalWrite(PIN_OUTPUT,setting);
  digitalWrite(LED_BUILTIN,setting);
}

void labelInit(){
  doPins(LABEL_UNDETECTED);
}

void labelDetected(){
  doPins(LABEL_DETECTED);
}

void labelUnDetected(){
  doPins(LABEL_UNDETECTED);
}

void setup() {
  pinMode(PIN_OUTPUT,OUTPUT);
  pinMode(PIN_INPUT,INPUT_PULLUP);
  pinMode(LED_BUILTIN,OUTPUT);
  labelInit();
}
int lbsPerSec = 10;

void loop() {
  if(!digitalRead(PIN_INPUT)){
    labelUnDetected();
    delayMS(milliSecBetweenLabels ); //(1000/(2*lbsPerSec));
    labelDetected();
    delayMS(8* milliSecBetweenLabels);
  }
}
