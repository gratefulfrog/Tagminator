#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

// Coms
//#define BAUD_RATE (9600)

// PINS
#define PIN_OUTPUT (3)
#define PIN_INPUT  (5)

// Control
#define LABEL_DETECTED   (1)
#define LABEL_UNDETECTED (0)
#define LABEL_INIT       (-1)

// Strings
#define STR_INIT     ("INIT")
#define STR_LABEL    ("Label: ")
#define STR_NO_LABEL ("NoLabel: ")


#endif
