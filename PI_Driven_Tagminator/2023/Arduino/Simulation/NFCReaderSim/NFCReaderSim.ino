#include <stdio.h>

#include "config.h"

volatile int counter   = 0,  // nb of labels counter
             iCounter = 0;   // nb of reads per label counter

const int nbReadsPerLabel = 20;

// a delay without call to 'delay()'
void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

// ISR 
void labelDetected(){
  counter++;
  iCounter = 0; 
  digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
}

void setup() {
  pinMode(PIN_INPUT,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_INPUT), labelDetected, RISING);
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.begin(BAUD_RATE);
  while(!Serial);
}

void loop() {
  //static long labelCount = 0;  // keep count of labels
  
  // use these local versions to decouple from interrupts during the loop
  int localCounter = counter,
      localICounter = iCounter;
  static char sn[1+SN_LENGTH],
              tp[1+TAMPERED_LENGTH],
              ic[1+I_COUNTER_LENGTH];

  if (localICounter == 0){
    delayMS(50);
    //Serial.print("Label: ");
    //Serial.println(labelCount++);
  }
  
  sprintf(ic, STR_I_COUNTER_FORMAT, iCounter++);
  sprintf(sn, STR_SN_FORMAT, STR_SN_PREFIX, localCounter);
  sprintf(tp, STR_TAMPERED_FORMAT,localCounter%2 ? TAMPERED : NOT_TAMPERED);

  // every 5 no tag is read 
  if(localCounter%5){
    Serial.print(ic);
    Serial.print(": ");
    Serial.print(sn);
    Serial.print(",");
    Serial.println(tp);
  }
  delayMS(1000/nbReadsPerLabel);  // approx number of reads per label
}
