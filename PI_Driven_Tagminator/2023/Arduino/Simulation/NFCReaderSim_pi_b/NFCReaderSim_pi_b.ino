#include <stdio.h>
#include "config.h"

// a delay without call to 'delay()'
void delayMS(double ms){
  double now = millis();
  while (millis()-now < ms);
}

void setup() {
  pinMode(PIN_INPUT,INPUT_PULLUP);
  pinMode(DISABLE_TAMPER_DETECT_PIN,INPUT_PULLUP);
  pinMode(LED_BUILTIN,OUTPUT);
  pinMode(PIN_OUTPUT,OUTPUT);
  digitalWrite(PIN_OUTPUT,HIGH);
  
  Serial.begin(BAUD_RATE);
  while(!Serial);
}

void printOutputs(char* sn, char* tp, char* ic){
  if (!digitalRead(DISABLE_TAMPER_DETECT_PIN)){
    Serial.println(sn);    
  }
  else{
    Serial.print(sn);
    Serial.print(" ");
    Serial.print(tp);
    Serial.print(" ");
    Serial.println(ic);
  }
}

void loop() {
  static int lastPinState = digitalRead(PIN_INPUT),
             iCounter     = 0;  // count nb reads
  static long  labelCount   = 0;  // keep count of labels
  static char sn[1+SN_LENGTH],
              tp[1+TAMPERED_LENGTH],
              ic[1+I_COUNTER_LENGTH];
  static bool init = false;

  if (! init){
    init = true;
    digitalWrite(PIN_OUTPUT,LOW);
  }
  
  int currentPinState = digitalRead(PIN_INPUT);
  
  
  if (currentPinState != lastPinState){
    digitalWrite(LED_BUILTIN,currentPinState );
    lastPinState = currentPinState;
    if (!currentPinState){
      labelCount++;
      iCounter = 0;
    }
  }
  if (!currentPinState && labelCount%5){
    sprintf(ic, STR_I_COUNTER_FORMAT, iCounter++);
    sprintf(sn, STR_SN_FORMAT, STR_SN_PREFIX, labelCount);
    sprintf(tp, STR_TAMPERED_FORMAT,labelCount%2 ? TAMPERED : NOT_TAMPERED);
    printOutputs(sn,tp,ic);
    /*Serial.print(sn);
    Serial.print(" ");
    Serial.print(tp);
    Serial.print(" ");
    Serial.println(ic);
    */
  }
  //delayMS(100);  // needed for fast boards
}
