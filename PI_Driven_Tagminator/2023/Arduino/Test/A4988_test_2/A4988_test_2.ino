/*Example sketch to control a stepper motor with A4988 stepper motor driver  no delay*/

// Define stepper motor connections and steps per revolution:
#define dirPin             (2)
#define stepPin            (3)
#define enablePin          (4)
#define stepsPerRevolution (100000) //(200) //2038

#define pulseTimeUS   (10)
#define pulsePauseMS  (1500)

unsigned long pt = pulseTimeUS,
              pp = pulsePauseMS;

const unsigned int minPP = 1500;

unsigned int ppUSFromSpeed(double speedRps,double ohUSPerStep){
  return (unsigned int) max(minPP,(((1000000/(stepsPerRevolution*speedRps/1000000))/1000000)-ohUSPerStep));
}
             
void setDirection(bool cw){
  int val = cw ? HIGH : LOW;
  digitalWrite(dirPin, val);
  digitalWrite(LED_BUILTIN,val);
}

void setup() {
  // Declare pins as output:
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  digitalWrite(enablePin,LOW);
  Serial.begin(115200);
}

void turn1Rev(unsigned long pUS,unsigned long dUS){
  unsigned long now ;
  if (dUS>10000){
    unsigned long dMS = dUS/1000.;
    now = micros();
    for (int i = 0; i < stepsPerRevolution; i++) {
      stepSansMS(pUS,dMS);
    }
  }
  else{
    now = micros();
    for (int i = 0; i < stepsPerRevolution; i++) {
      stepSansUS(pUS,dUS);
    }
  }
  unsigned long delta = micros()-now;
  double rps = 1000000.0/(double)delta;
  double usPerStep = (double)delta/(double)stepsPerRevolution;
  double overheadPCT = 100*(usPerStep - (double)dUS)/usPerStep; ///(double)dUS;
  double ohUSPerStep = (usPerStep - (double)dUS);
  Serial.print("Delta µs: ");
  Serial.print((double)delta);
  Serial.print(" Rev/s: ");
  Serial.print(rps);
  Serial.print(" µs/step: ");
  Serial.print(delta/200.);
  Serial.print(" overhead us/step: ");
  Serial.print(ohUSPerStep);
  Serial.print(" pause in us: ");
  Serial.print(((1000000/(200*rps/1000000))/1000000)-ohUSPerStep);
  unsigned int pp = ppUSFromSpeed(rps,ohUSPerStep);
  Serial.print(" Computed pause in us: ");
  Serial.println(((1000000/(200*rps/1000000))/1000000)-ohUSPerStep);
  
}

void stepSansUS(unsigned long pUS,unsigned long dUS){
  // These four lines result in 1 step:
  unsigned long limit = pUS + dUS,
           now = micros();
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(pUS);
  digitalWrite(stepPin, LOW);
  while (micros()-now < limit);
}
void stepSansMS(unsigned long pUS,unsigned long dMS){
  // These four lines result in 1 step:
  unsigned long limit = dMS,
           now = millis();
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(pUS);
  digitalWrite(stepPin, LOW);
  while (millis()-now < limit);
}


void loop() {
  
  if (Serial.available()>0){
    float fpp = Serial.parseFloat();
    Serial.print("\nValue read: ");
    Serial.print((double)(fpp));
    pp = ppUSFromSpeed((double)fpp,25);
    Serial.print(" New pulse pause US: ");
    Serial.println(pp);
  }
  
  // Set the spinning direction clockwise:
  setDirection(true);
  turn1Rev(pt,pp);
/*
  delay(1000);

  // Set the spinning direction counterclockwise:
  setDirection(false);  
  turn1Rev(pt,pp);
  
  delay(500);
  */
}
