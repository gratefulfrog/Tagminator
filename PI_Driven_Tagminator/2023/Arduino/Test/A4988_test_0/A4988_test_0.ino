/*Example sketch to control a stepper motor with A4988 stepper motor driver */

// Define stepper motor connections and steps per revolution:
#define dirPin 2
#define stepPin 3
#define stepsPerRevolution 2038

void setDirection(bool cw){
  int val = cw ? HIGH : LOW;
  digitalWrite(dirPin, val);
  digitalWrite(LED_BUILTIN,val);
}

void setup() {
  // Declare pins as output:
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
}
void step(int delMS){
  // These four lines result in 1 step:
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(delMS);
  digitalWrite(stepPin, LOW);
  delayMicroseconds(delMS);
}

void turn1Rev(int delMS){
  for (int i = 0; i < stepsPerRevolution; i++) {
    step(delMS);
  }
}

void loop() {
  // Set the spinning direction clockwise:
  setDirection(true);
  
  // Spin the stepper motor 1 revolution slowly:
  turn1Rev(2000);

  delay(500);

  // Set the spinning direction counterclockwise:
  setDirection(false);
  
  // Spin the stepper motor 1 revolution quickly:
  turn1Rev(1000);
  
  delay(500);
}
