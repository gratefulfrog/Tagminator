/*Example sketch to control a stepper motor with A4988 stepper motor driver */

// Define stepper motor connections and steps per revolution:
#define dirPin (2)
#define stepPin (3)
#define stepsPerRevolution (200) //2038

#define pulseTimeUS   (1)
#define pulsePause    (20000)

unsigned int pt = pulseTimeUS,
             pp = pulsePause;

const unsigned int minPP = 2200;

unsigned int ppUSFromSpeed(double speedRps,double ohUSPerStep){
  return (unsigned int) max(minPP,(((1000000/(stepsPerRevolution*speedRps/1000000))/1000000)-ohUSPerStep));
}
             
void setDirection(bool cw){
  int val = cw ? HIGH : LOW;
  digitalWrite(dirPin, val);
  digitalWrite(LED_BUILTIN,val);
}

void setup() {
  // Declare pins as output:
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  Serial.begin(115200);
}
void stepUS(unsigned int pUS,unsigned int dUS){
  // These four lines result in 1 step:
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(pUS);
  digitalWrite(stepPin, LOW);
  delayMicroseconds(dUS);
}
void stepMS(unsigned int pUS,unsigned int dMS){
  // These four lines result in 1 step:
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(pUS);
  digitalWrite(stepPin, LOW);
  delay(dMS);
}

void turn1Rev(unsigned int pUS,unsigned int dUS){
  unsigned long now ;
  if (dUS>10000){
    unsigned int dMS = dUS/1000.;
    now = micros();
    for (int i = 0; i < stepsPerRevolution; i++) {
      stepMS(pUS,dMS);
    }
  }
  else{
    now = micros();
    for (int i = 0; i < stepsPerRevolution; i++) {
      stepUS(pUS,dUS);
    }
  }
  unsigned long delta = micros()-now;
  double rps = 1000000.0/(double)delta;
  double usPerStep = (double)delta/(double)stepsPerRevolution;
  double overheadPCT = 100*(usPerStep - (double)dUS)/usPerStep; ///(double)dUS;
  double ohUSPerStep = (usPerStep - (double)dUS);
  Serial.print("Delta µs: ");
  Serial.print((double)delta);
  Serial.print(" Rev/s: ");
  Serial.print(rps);
  Serial.print(" µs/step: ");
  Serial.print(delta/200.);
  Serial.print(" overhead us/step: ");
  Serial.print(ohUSPerStep);
  Serial.print(" pause in us: ");
  Serial.print(((1000000/(200*rps/1000000))/1000000)-ohUSPerStep);
  unsigned int pp = ppUSFromSpeed(rps,ohUSPerStep);
  Serial.print(" Computed pause in us: ");
  Serial.println(((1000000/(200*rps/1000000))/1000000)-ohUSPerStep);
  
}

void loop() {

  if (Serial.available()>0){
    float fpp = Serial.parseFloat();
    Serial.print("\nValue read: ");
    Serial.print((double)(fpp));
    pp = ppUSFromSpeed((double)fpp,25);
    Serial.print(" New pulse pause US: ");
    Serial.println(pp);
  }
  // Set the spinning direction clockwise:
  setDirection(true);
  
  // Spin the stepper motor 1 revolution slowly:
  turn1Rev(pt,pp);

  delay(1000);

  // Set the spinning direction counterclockwise:
  setDirection(false);
  
  // Spin the stepper motor 1 revolution quickly:
  turn1Rev(pt,pp);
  
  delay(500);
}
