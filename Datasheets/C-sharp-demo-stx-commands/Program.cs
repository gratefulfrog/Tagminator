﻿/**
 * Simple C# STX/ETX Communication Example using SerialPort Class
 * 
 * @author C. Pauls
 * @date 2011-04-14
 */

using System;
using System.IO.Ports;

namespace contest
{
    class Program
    {

        static SerialPort port;

        /* Control Character */
        const char STX = '\x02';
        const char ETX = '\x03';
        const char ACK = '\x06';
        const char ESC = '\x1b';

        static void Main(string[] args)
        {
            String cmd;
            String portname;

            Console.Write("Port: ");
            portname = Console.ReadLine();

            port = new SerialPort(portname);

            port.DtrEnable = true; // This is important for use with virtual serial ports over USB!
            port.Open();

            port.Write(ESC.ToString());

            while(true) {
                Console.Write("Command (or ENTER to quit): ");
                cmd = Console.ReadLine();
                if (cmd == "") break;
                Console.WriteLine(stxetxRequest(cmd));
            } 
            port.Close();
        }

        /**
         * Send STX/ETX Command to Reader
         * 
         * Limitations:
         *  - Response checksum is not validated
         *  - No exceptions are caught
         *  
         * @param cmd Command String
         * @return Response
         */
        static String stxetxRequest(String cmd) {
            String ret;
            int i;
            char checksum = '\x01'; // Preset checksum with STX xor ETX.
            char[] a;

            /* Calculate XOR Checksum */
            a = cmd.ToCharArray();
            for (i = 0; i < cmd.Length; i++)
            {
                checksum ^= a[i];
            }

            /* Send Command to Reader */
            port.Write(STX + cmd + ETX + checksum);

            /* Read Response */
            ret = port.ReadTo(ETX.ToString()); // Read Response
            i = port.ReadChar(); // read Checksum
            
            /* Validate Response */
            if (!ret.StartsWith(ACK.ToString()))
            {
                Console.WriteLine("STXETX Error!");
            }
            return (ret.Substring(2,ret.Length-2));
        }
    }
}
