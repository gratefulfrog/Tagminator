#!/usr/bin/python3

import sys
import time
import random
import pigpio


def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      exit()
   return pig

if __name__ == '__main__':
   pig = getPig()

   # step pin
   pig.set_mode(24, pigpio.OUTPUT)
   # dir pin
   pig.set_mode(25, pigpio.OUTPUT)
   pig.write(25,0)
   maxStepsSec = 0
   minStepsSec = 1000
   
   flash_100=[] 
   #                              ON     OFF  DELAY
   flash_100.append(pigpio.pulse(1<<24,   0    , 10))
   flash_100.append(pigpio.pulse(0,      1<<24 , 20000))

   pig.wave_clear() # clear any existing waveforms
      
   pig.wave_add_generic(flash_100) # 100 ms flashes
   f100 = pig.wave_create() # create and save id

   
   pig.wave_send_repeat(f100)
   time.sleep(10)
   pig.wave_tx_stop() # stop waveform
   pig.wave_clear() # clear all waveforms
   sys.exit(0)
   
   nbRevs = 1
   m256 = (nbRevs*200//256)
   n200 = nbRevs*200 - m256*256
   print(n200,m256)
   maxStepsSec = 0
   minStepsSec = 1000
   #while True:
   now = time.time()
   pig.wave_chain([
      255, 0,                       # loop start
      f100,                      # transmit the wave
      255, 1, n200, m256,               # loop end (repeat nbRevs times)
   ])
   stepsSec = 200/(time.time()-now)
   minStepsSec = min(minStepsSec,stepsSec)
   maxStepsSec = max(maxStepsSec,stepsSec)
   print(round(stepsSec),round(minStepsSec),round(maxStepsSec), 'current, min, max steps/sec')

   time.sleep(0.1)
   pig.write(25,not pig.read(25))
   #while pi.wave_tx_busy():
   #pass #time.sleep(0.1);

   sys.exit(0)      
