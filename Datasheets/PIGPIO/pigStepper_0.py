#!/usr/bin/python3
import sys
import time
import pigpio

def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      sys.exit()
   return pig

if __name__ == '__main__':
   pig = getPig()

   # step pin
   pig.set_mode(24, pigpio.OUTPUT)
   # dir pin
   pig.set_mode(25, pigpio.OUTPUT)
   pig.write(25,0)
   maxStepsSec = 0
   minStepsSec = 1000
   while True:
      now = time.time()
      for i in range(200):
         pig.gpio_trigger(24,1,1)
         time.sleep(0.001)
      stepsSec = 200/(time.time()-now)
      minStepsSec = min(minStepsSec,stepsSec)
      maxStepsSec = max(maxStepsSec,stepsSec)
      print(round(stepsSec),round(minStepsSec),round(maxStepsSec), 'current, min, max steps/sec')
      pig.write(25,not pig.read(25))
   
