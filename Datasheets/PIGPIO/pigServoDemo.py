#!/usr/bin/python3

# servo_demo.py
# 2016-10-07
# Public Domain

# servo_demo.py          # Send servo pulses to GPIO 4.
# servo_demo.py 23 24 25 # Send servo pulses to GPIO 23, 24, 25.

import sys
import time
import random
import pigpio

NUM_GPIO=32

MIN_WIDTH=700
MAX_WIDTH=2200

step = [0]*NUM_GPIO
width = [0]*NUM_GPIO
used = [False]*NUM_GPIO
G=[]

def sweep(pi):
   print("Sending servos pulses to GPIO {}, control C to stop.".
         format(' '.join(str(g) for g in G)))
   #pi.set_PWM_frequency(G[0],500)
   print(pi.get_PWM_frequency(G[0]))
         
   while True:
      try:
         for g in G:
            pi.set_servo_pulsewidth(g, width[g])
            print(g, width[g])
            #print(pi.get_PWM_frequency(G[0]))
            width[g] += step[g]
            if width[g]<MIN_WIDTH or width[g]>MAX_WIDTH:
               step[g] = -step[g]
               width[g] += step[g]
         time.sleep(0.01)
      except KeyboardInterrupt:
         break
   print("\nTidying up")
   for g in G:
      pi.set_servo_pulsewidth(g, 0)
   pi.stop()

def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      exit()
   return pig

def setup():
   global G
   if len(sys.argv) == 1:
      G = [4]
   else:
      G = []
      for a in sys.argv[1:]:
         G.append(int(a))
   
   for g in G:
      used[g] = True
      step[g] = 50  # random.randrange(5, 25)
      width[g] = (MIN_WIDTH+MAX_WIDTH)/2

if __name__ == '__main__':
   pig = getPig()
   setup()
   sweep(pig)
