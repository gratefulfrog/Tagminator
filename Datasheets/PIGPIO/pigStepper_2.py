#!/usr/bin/python3
import sys
import time
import pigpio

def micros():
    return 1000*time.perf_counter_ns()
def nanos():
    return time.perf_counter_ns()

def delayMicroseconds(pulseWidth):
    pw = 1000*pulseWidth
    now = nanos()
    while (nanos()-now < pw):
        pass

def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      sys.exit()
   return pig

def pulsePinTrigger(pig,pin,us,dir):
    pig.gpio_trigger(pin,us,dir)

def pulsePinWrite(pig, pin,us,dir):
    pig.write(pin,dir)
    delayMicroseconds(us)
    pig.write(pin,not dir)

if __name__ == '__main__':
    pig = getPig()
    stepsPerRev  = 8000
    pulseLenUS   = 10
    pulseDelayUS = 1500
    pulseDirection = 1

    if len(sys.argv) == 5:
        [stepsPerRev,pulseLenUS,pulseDelayUS,pulseDirection] = map(int,sys.argv[1:])
        
    print(  'steps/Rev      =', stepsPerRev, 
          '\npulseLenUS     =', pulseLenUS, 
          '\npulseDelayUS   =', pulseDelayUS, 
          '\npulseDirection =',pulseDirection)
 
    # step pin
    pig.set_mode(24, pigpio.OUTPUT)
    # dir pin
    pig.set_mode(25, pigpio.OUTPUT)
    pig.write(25,0)
    pig.set_mode(5, pigpio.OUTPUT)
    pig.write(5,0)
    maxStepsSec = 0
    minStepsSec = 2000
    pulseFunc = pulsePinTrigger if pulseLenUS <= 100 else pulsePinWrite
    while True:
        now = time.time()
        pig.write(5,0)
        for i in range(stepsPerRev):
            pulseFunc(pig,24,pulseLenUS,pulseDirection)
            delayMicroseconds(pulseDelayUS)
        pig.write(5,1)
        stepsSec = stepsPerRev/(time.time()-now)
        minStepsSec = min(minStepsSec,stepsSec)
        maxStepsSec = max(maxStepsSec,stepsSec)
        print(round(stepsSec),round(minStepsSec),round(maxStepsSec), 'current, min, max steps/sec')
        pig.write(25,not pig.read(25))
        time.sleep(1)
   
# ./pigStepper_2.py 200 10 600 1
