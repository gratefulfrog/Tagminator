#!/usr/bin/python3

import sys
import time
import random
import pigpio


def getPig():
   pig = pigpio.pi()
   if not pig.connected:
      exit()
   return pig

if __name__ == '__main__':
   pig = getPig()
   
   stepsPerRev  = 200
   pulseLenUS   = 10
   pulseDelayUS = 1000
   pulseDirection = 1
   stepPin   = 24
   dirPin   = 25
   enablePin = 5
   
   if len(sys.argv) == 5:
      [stepsPerRev,pulseLenUS,pulseDelayUS,pulseDirection] = map(int,sys.argv[1:])
      
   print(  'steps/Rev      =', stepsPerRev, 
           '\npulseLenUS     =', pulseLenUS, 
           '\npulseDelayUS   =', pulseDelayUS, 
           '\npulseDirection =',pulseDirection)

   # step pin
   pig.set_mode(stepPin, pigpio.OUTPUT)
   # dir pin
   pig.set_mode(dirPin, pigpio.OUTPUT)
   pig.write(25,0)
   # enable pin
   pig.set_mode(enablePin, pigpio.OUTPUT)
   pig.write(enablePin,1)


   flash=[] 
   #                         ON     OFF   DELAY
   flash.append(pigpio.pulse(1<<24, 0     , pulseLenUS))   #if pulseDirection else pulseDelayUS))
   flash.append(pigpio.pulse(0,     1<<24 , pulseDelayUS)) # if pulseDirection else pulseLenUS))

   pig.wave_clear() # clear any existing waveforms

   pig.wave_add_generic(flash) # 100 ms flashes
   f100 = pig.wave_create() # create and save id

   try:
      pig.write(enablePin, 0)
      pig.wave_send_repeat(f100)
      while True:
         pass
   except KeyboardInterrupt:
      pig.write(enablePin, 1)
      pig.wave_tx_stop()
      pig.stop()
